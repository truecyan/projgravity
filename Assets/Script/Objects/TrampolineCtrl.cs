﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrampolineCtrl : MonoBehaviour
{
	[HideInInspector] public SpriteRenderer Renderer;
	public Sprite ActiveSprite;
	public Sprite InactiveSprite;
	public float BounceForce = 36.0f;
	
	// Use this for initialization
	void Start ()
	{
		Renderer = gameObject.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Activate()
	{
		Renderer.sprite = ActiveSprite;
	}

	public void Deactivate()
	{
		Renderer.sprite = InactiveSprite;
	}
}
