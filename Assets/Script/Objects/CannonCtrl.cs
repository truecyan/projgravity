﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CannonCtrl : MonoBehaviour
{
	
	[Header("Cannon Properties")]
	public float LaunchForce = 20f;

	[Header("Do Not Modify")]
	public Image Slider;
	public GameObject LoadedBox;

	private float _rotConstant = 0.6f;
	private bool _loaded;
	
	private bool _charge = true;
	private float _chargeTime = 2f;

	public static bool _doTutorial = true;

	//public delegate void CannonLaunch();
	//public static event CannonLaunch DoLaunch;

	public static UnityEvent DoLaunch = new UnityEvent();
	
	// Use this for initialization
	void Start ()
	{
		ObjectCtrl.OnCannonLoaded.AddListener(OnCannonLoaded);
	}
	
	// Update is called once per frame
	void Update () {
		if (LoadedBox != null && _loaded)
		{
			//Debug.Log(transform.GetChild(0).rotation.eulerAngles.z -_rotConstant*Input.GetAxis("Horizontal"));
			float toAngle = transform.GetChild(0).localRotation.eulerAngles.z - _rotConstant * Input.GetAxis("Horizontal");
			if (toAngle < 90f || toAngle > 270f)
				transform.GetChild(0).Rotate(0,0,-_rotConstant*Input.GetAxis("Horizontal"));
			else if (toAngle < 91f)
				transform.GetChild(0).localRotation = Quaternion.Euler(0,0,90f);
			else if (toAngle > 269f)
				transform.GetChild(0).localRotation = Quaternion.Euler(0,0,270f);

			if (Input.GetButton("Jump"))
			{
				if (_charge)
				{
					Slider.fillAmount += 1 / _chargeTime * Time.deltaTime;
					if (Slider.fillAmount >= 1)
						_charge = false;
				}
				else
				{
					Slider.fillAmount -= 1 / _chargeTime * Time.deltaTime;
					if (Slider.fillAmount <= 0)
						_charge = true;
				}
			}

			if (Input.GetButtonUp("Jump"))
			{
				LoadedBox.GetComponent<Collider2D>().enabled = true;
				LoadedBox.transform.parent = null;
				if (DoLaunch != null) DoLaunch.Invoke();
				Debug.Log(LoadedBox.transform.up*LaunchForce*Slider.fillAmount);
				LoadedBox.GetComponent<Rigidbody2D>().AddForce(LoadedBox.transform.up*LaunchForce*Slider.fillAmount,ForceMode2D.Impulse);
				LoadedBox.GetComponent<SpriteRenderer>().sortingLayerName = "Box";
				LoadedBox = null;
				Slider.fillAmount = 0;
				transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
			}
		}
	}

	public void OnCannonLoaded()
	{
		_loaded = true;
		if(LoadedBox != null)
			transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
		if (_doTutorial)
		{
			SysMsgCtrl.SystemMsg("방향키로 각도를 조절하고, \n Space키를 꾹 눌러 세기를 조절합니다.", SysMsgCtrl.DesMode.AfterSeconds);
			_doTutorial = false;
		}
	}
}
