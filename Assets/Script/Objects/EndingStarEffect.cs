﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingStarEffect : MonoBehaviour
{
	[HideInInspector] public SpriteRenderer SpriteRenderer;
	public float Period = 4;
	private float _lLimit = 0.3f;
	private float _mLimit = 1;
	private float _phase = 0;

	// Use this for initialization
	void Start ()
	{
		SpriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		Color c = SpriteRenderer.color;
		_phase += Mathf.PI * 2 / Period * Time.deltaTime;
		SpriteRenderer.color = new Color(c.r,c.g,c.b,_lLimit+(_mLimit-_lLimit)*(Mathf.Sin(_phase)+1)/2);
	}
}
