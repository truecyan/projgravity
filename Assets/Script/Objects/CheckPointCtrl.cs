﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting;
using UnityEngine;
using UnityEngine.Events;

public class CheckPointCtrl : MonoBehaviour
{

	[HideInInspector] public SpriteRenderer Renderer;
	public static UnityEvent OnSave = new UnityEvent();
	public Sprite InactiveSprite;
	public Sprite ActiveSprite;
	public bool Active;
	public static bool Saved;
	
	
	// Use this for initialization
	void Start ()
	{
		Renderer = gameObject.GetComponent<SpriteRenderer>();
		OnSave.AddListener(OnChecking);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnDestroy()
	{
		OnSave.RemoveListener(OnChecking);
	}

	public void Checkpoint()
	{
		if (!Active)
		{
			Debug.Log("Checked");
			Saved = true;
			OnSave.Invoke();
			Renderer.sprite = ActiveSprite;
			Active = true;
		}
		
	}

	private void OnChecking()
	{
		Active = false;
		Renderer.sprite = InactiveSprite;
	}
}
