﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndPointCtrl : MonoBehaviour
{
	[HideInInspector] public SpriteRenderer Renderer;
	//public string NextScene;
	//public Sprite ActiveSprite;
	// Use this for initialization
	void Start ()
	{
		Renderer = gameObject.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void StageEnd()
	{
		CheckPointCtrl.Saved = false;
		int result = 2;
		if (GameObject.FindGameObjectsWithTag("Starlight").Length == 0) result = 3;
		GameManager.Instance.StageExit(result);
		Initiate.Fade("StageSelect", Color.white, 1f);
	}

	IEnumerator ToNextScene()
	{
		yield return new WaitForSeconds(1.0f);
		CheckPointCtrl.Saved = false;
		Initiate.Fade("StageSelect", Color.white, 1f);
	}
}
