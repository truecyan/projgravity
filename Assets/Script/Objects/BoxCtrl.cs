﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BoxCtrl : MonoBehaviour {
[HideInInspector] public GameObject[] Planets;
	[HideInInspector] public Rigidbody2D Rigidbody;
	public GameObject CurrPlanet;
	
	[Header("Object Properties")]
	public float Mass = 1f;
	public bool Hostile = false;
	public bool Damage = false;
	public float DamageAmount = 0f;

	private Vector3 _savedPos;
	private Quaternion _savedRot;
	private Vector2 _savedVel;
	
	[SerializeField] private bool _colCheck;
	private bool _colPlanet;
	private bool _colObject;
	private bool _freeze;

	private bool _onTrampoline;
	private GameObject _trampoline;
	private float _tramForce;
	
	private bool _canMove;
	private Vector2 _prevVel;
	
	
	//public delegate void Cannon();
	//public static event Cannon OnCannonLoaded;

	public static UnityEvent OnCannonLoaded = new UnityEvent();

	
	// Use this for initialization
	void Start () {
		//맵에 존재하는 모든 행성 받아오기
		Planets = GameObject.FindGameObjectsWithTag("Planet");
		Rigidbody = gameObject.GetComponent<Rigidbody2D>();
		Rigidbody.mass = Mass;
	}
	
	
	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Cannon"))
		{
			gameObject.GetComponent<Collider2D>().enabled = false;
			gameObject.transform.parent = other.gameObject.transform.GetChild(0);
			gameObject.transform.localPosition = new Vector3(0,4f,0);
			gameObject.transform.localRotation = Quaternion.identity;
			gameObject.GetComponent<SpriteRenderer>().sortingLayerName = "LoadedBox";
			Rigidbody.velocity = Vector2.zero;
			
			_colCheck = false;
			if (other.gameObject.CompareTag("PlanetCollider"))
			{
				CurrPlanet = null;
			}
			_freeze = true;
			other.gameObject.GetComponent<CannonCtrl>().LoadedBox = gameObject;
			CameraMovement.Cannon = other.gameObject;
			if (OnCannonLoaded != null) OnCannonLoaded.Invoke();
		}
		else if (other.gameObject.CompareTag("Trampoline"))
		{
			if (!_colCheck)
			{
				_onTrampoline = true;
				_trampoline = other.gameObject;
				TrampolineCtrl tram = _trampoline.GetComponent<TrampolineCtrl>();
				tram.Activate();
				_tramForce = tram.BounceForce;
			}
		}
	}
	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.gameObject.CompareTag("Trampoline"))
		{
			_onTrampoline = false;
			collision.gameObject.GetComponent<TrampolineCtrl>().Deactivate();
		}
	}
	private void OnCollisionEnter2D(Collision2D other)
	{
		if (_freeze || other.gameObject.CompareTag("Player")) return;
		//Debug.Log("True");
		_colCheck = true;

		if (other.gameObject.CompareTag("PlanetCollider"))
		{
			_colPlanet = true;
			CurrPlanet = other.gameObject;
			if (_onTrampoline)
			{
				Rigidbody.AddForce(_trampoline.transform.up*_tramForce, ForceMode2D.Impulse);
				_onTrampoline = false;
			}
		}
		else if (other.gameObject.CompareTag("Box"))
		{
			_colObject = true;
			if(other.gameObject.GetComponent<ObjectCtrl>().CurrPlanet != null)
				CurrPlanet = other.gameObject.GetComponent<ObjectCtrl>().CurrPlanet;
		}
	}

	private void OnCollisionStay2D(Collision2D other)
	{
		if (_freeze || other.gameObject.CompareTag("Player")) return;
		if (CurrPlanet == null && other.gameObject.CompareTag("Box"))
		{
			CurrPlanet = other.gameObject.GetComponent<ObjectCtrl>().CurrPlanet;
		}
	}

	private void OnCollisionExit2D(Collision2D other)
	{
		if (other.gameObject.CompareTag("Player")) return;
		
		_colCheck = false;
		if (other.gameObject.CompareTag("PlanetCollider"))
		{
			_colPlanet = false;
			CurrPlanet = null;
		}

		if (other.gameObject.CompareTag("Box"))
		{
			_colObject = false;
		}

		if (_colPlanet || _colObject)
		{
			_colCheck = true;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		
	}

	float Sqr(float a)
	{
		return a * a;
	}


	float ProjectionMag(Vector2 a, Vector2 to)
	{
		return Vector2.Dot(a, to) / Vector2.Dot(to, to);
	}
}
