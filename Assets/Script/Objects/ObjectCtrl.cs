﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.XR.Interaction;

public class ObjectCtrl : MonoBehaviour {
	[HideInInspector] public GameObject[] Planets;
	[HideInInspector] public Rigidbody2D Rigidbody;
	public GameObject CurrPlanet;
	
	[Header("Object Properties")]
	public float Mass = 1f;
	public bool Hostile = false;
	public bool Damage = false;
	public float DamageAmount = 0f;
	
	[Header("Environment Constants")]
	public float GravityConstant = 2.0f;
	public float RotConstant = 5.0f;
	public float GroundRotConstant = 30.0f;

	private Vector2 _gravityForce = Vector2.zero;

	private Vector3 _savedPos;
	private Quaternion _savedRot;
	private Vector2 _savedVel;
	
	[SerializeField] private bool _colCheck;
	private bool _colPlanet;
	private bool _colObject;
	private bool _freeze = false;

	private bool _onTrampoline;
	private GameObject _trampoline;
	private float _tramForce;
	
	private bool _canMove;
	private Vector2 _prevVel;
	
	
	//public delegate void Cannon();
	//public static event Cannon OnCannonLoaded;

	public static UnityEvent OnCannonLoaded = new UnityEvent();

	
	// Use this for initialization
	void Start () {
		//맵에 존재하는 모든 행성 받아오기
		Planets = GameObject.FindGameObjectsWithTag("Planet");
		Rigidbody = gameObject.GetComponent<Rigidbody2D>();
		Rigidbody.mass = Mass;
		OnCannonLoaded.AddListener(FreezeStart);
		CannonCtrl.DoLaunch.AddListener(FreezeEnd);
		CheckPointCtrl.OnSave.AddListener(SaveStatus);
		PlayerMovement.OnReload.AddListener(ApplyStatus);
	}
	
	
	private void OnTriggerEnter2D(Collider2D other)
	{
		if (gameObject.CompareTag("Box") && other.gameObject.CompareTag("Cannon"))
		{
			gameObject.GetComponent<Collider2D>().enabled = false;
			gameObject.transform.parent = other.gameObject.transform.GetChild(0);
			gameObject.transform.localPosition = new Vector3(0,4f,0);
			gameObject.transform.localRotation = Quaternion.identity;
			gameObject.GetComponent<SpriteRenderer>().sortingLayerName = "LoadedBox";
			Rigidbody.velocity = Vector2.zero;
			//Debug.Log("False");
			_colCheck = false;
			if (other.gameObject.CompareTag("PlanetCollider"))
			{
				CurrPlanet = null;
			}
			_freeze = true;
			other.gameObject.GetComponent<CannonCtrl>().LoadedBox = gameObject;
			CameraMovement.Cannon = other.gameObject;
			if (OnCannonLoaded != null) OnCannonLoaded.Invoke();
		}
		else if (other.gameObject.CompareTag("Trampoline"))
		{
			if (!_colCheck)
			{
				_onTrampoline = true;
				_trampoline = other.gameObject;
				TrampolineCtrl tram = _trampoline.GetComponent<TrampolineCtrl>();
				tram.Activate();
				_tramForce = tram.BounceForce;
			}
		}
	}
	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.gameObject.CompareTag("Trampoline"))
		{
			_onTrampoline = false;
			collision.gameObject.GetComponent<TrampolineCtrl>().Deactivate();
		}
	}
	private void OnCollisionEnter2D(Collision2D other)
	{
		if (_freeze || other.gameObject.CompareTag("Player")) return;
		//Debug.Log("True");
		_colCheck = true;

		if (other.gameObject.CompareTag("PlanetCollider"))
		{
			_colPlanet = true;
			CurrPlanet = other.gameObject;
			if (_onTrampoline)
			{
				Rigidbody.AddForce(_trampoline.transform.up*_tramForce, ForceMode2D.Impulse);
				_onTrampoline = false;
			}
		}
		else if (other.gameObject.CompareTag("Box"))
		{
			_colObject = true;
			if(other.gameObject.GetComponent<ObjectCtrl>().CurrPlanet != null)
				CurrPlanet = other.gameObject.GetComponent<ObjectCtrl>().CurrPlanet;
		}
	}

	private void OnCollisionStay2D(Collision2D other)
	{
		if (_freeze || other.gameObject.CompareTag("Player")) return;
		if (CurrPlanet == null && other.gameObject.CompareTag("Box"))
		{
			CurrPlanet = other.gameObject.GetComponent<ObjectCtrl>().CurrPlanet;
		}
	}

	private void OnCollisionExit2D(Collision2D other)
	{
		if (other.gameObject.CompareTag("Player")) return;
		
		_colCheck = false;
		if (other.gameObject.CompareTag("PlanetCollider"))
		{
			_colPlanet = false;
			CurrPlanet = null;
		}

		if (other.gameObject.CompareTag("Box"))
		{
			_colObject = false;
		}

		if (_colPlanet || _colObject)
		{
			_colCheck = true;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (_freeze) return;
		//중력 계산
		Vector2 totalForce = Vector2.zero;
		foreach (var planet in Planets)
		{
			Vector2 pos = planet.transform.position;
			float planetMass = planet.GetComponent<AsteroidProperties>().Mass;
			Vector2 distVector = pos - Rigidbody.position;
			totalForce += GravityConstant*distVector.normalized*planetMass*Mass/Sqr(distVector.magnitude);
		}

		_gravityForce = totalForce;
		
		//상자 기준 벡터 (앞, 아래)
		Vector3 fwdVector;
		Vector3 dwnVector;
		
		//상자 바닥 정하기
		Vector2 projVectorX = Vector2.Dot(transform.right, totalForce) * transform.right;
		Vector2 projVectorY = totalForce - projVectorX;
		if (projVectorX.magnitude < projVectorY.magnitude)
		{
			if (Vector2.Dot(projVectorY,transform.up) < 0)
			{
				fwdVector = transform.right;
				dwnVector = -transform.up;
			}
			else
			{
				fwdVector = -transform.right;
				dwnVector = transform.up;
			}
		}
		else
		{
			if (Vector2.Dot(projVectorX,transform.right) < 0)
			{
				fwdVector = -transform.up;
				dwnVector = -transform.right;
			}
			else
			{
				fwdVector = transform.up;
				dwnVector = transform.right;
			}
		}
		
		//중력 적용
		Vector3 forceVector = totalForce;
		
		//과도한 회전을 막기 위한 벡터(초기, z값)
		float initCross = Vector3.Cross(dwnVector, forceVector).z;

		//회전
		float rot;
		if (_colCheck)
			rot = Vector2.Dot(totalForce, fwdVector)*GroundRotConstant*Time.deltaTime;
		else
			rot = Vector2.Dot(totalForce, fwdVector)*RotConstant*Time.deltaTime;
		//transform.Rotate(0f,0f,rot);
		
		//과도하게 회전할 시 중력 방향으로 플레이어의 회전방향을 맞춤
		float newCross = Vector3.Cross(dwnVector, forceVector).z;
		if (initCross * newCross < 0)
		{
			float nowAngle = Mathf.Atan2(dwnVector.y, dwnVector.x) * Mathf.Rad2Deg;
			float toAngle = Mathf.Atan2(forceVector.y, forceVector.x) * Mathf.Rad2Deg;
			rot = toAngle - nowAngle;
			//transform.Rotate(0f,0f,rot);
		}
		
		Rigidbody.AddForce(totalForce);
	}

	private void OnDestroy()
	{
		OnCannonLoaded.RemoveListener(FreezeStart);
		CannonCtrl.DoLaunch.RemoveListener(FreezeEnd);
		CheckPointCtrl.OnSave.RemoveListener(SaveStatus);
	}

	float Sqr(float a)
	{
		return a * a;
	}

	private void FreezeStart()
	{
		_prevVel = Rigidbody.velocity;
		Rigidbody.velocity = Vector2.zero;
		_freeze = true;
	}

	private void FreezeEnd()
	{
		Rigidbody.velocity = _prevVel;
		_freeze = false;
	}

	private void SaveStatus()
	{
		_savedPos = transform.position;
		_savedRot = transform.rotation;
		_savedVel = Rigidbody.velocity;
	}
	private void ApplyStatus()
	{
		transform.position = _savedPos;
		transform.rotation = _savedRot;
		Rigidbody.velocity = _savedVel;
	}

	float ProjectionMag(Vector2 a, Vector2 to)
	{
		return Vector2.Dot(a, to) / Vector2.Dot(to, to);
	}
}

