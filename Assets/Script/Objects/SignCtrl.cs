﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignCtrl : MonoBehaviour {

	public string Message = "";

	public Sprite SpriteActive;
	public Sprite SpriteInactive;

	[Header("Do not touch")]

	public GameObject Canvas;
	public Text MsgText;

	[HideInInspector] public GameObject Camera;
	[HideInInspector] public SpriteRenderer Renderer;

	// Use this for initialization
	void Start () {
		Renderer = gameObject.GetComponent<SpriteRenderer>();
		Renderer.sprite = SpriteInactive;
		Camera = GameObject.FindGameObjectWithTag("MainCamera");
		MsgText.text = Message;
	}
	
	// Update is called once per frame
	void Update () {
		if (Canvas.activeSelf)
		{
			Canvas.transform.rotation = Camera.transform.rotation;
		}
	}

	public void Activate()
	{
		Renderer.sprite = SpriteActive;
		Canvas.SetActive(true);
	}

	public void Deactivate()
	{
		Renderer.sprite = SpriteInactive;
		Canvas.SetActive(false);
	}

}
