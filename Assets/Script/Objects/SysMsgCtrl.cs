﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SysMsgCtrl : MonoBehaviour
{
	public enum DesMode { Move, AfterSeconds }
	public DesMode DestroyMode = DesMode.AfterSeconds; 
	public float WaitingSeconds = 2.0f;
	public string Message = "";
	
	[Header("Do not touch")]
	public Image Background;
	public Image Frame;
	public Text Text;

	void Awake()
	{
	}
	// Use this for initialization
	void Start ()
	{
		Text.text = Message;
		if(DestroyMode == DesMode.AfterSeconds)
		StartCoroutine("FadeIn");
	}
	
	// Update is called once per frame
	void Update () {

		if(DestroyMode == DesMode.Move){
			
			if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0.01f)
			{
				StartCoroutine("FadeOut");
			}
		}
	}

	IEnumerator FadeIn()
	{
		for (float alpha = 0f; alpha <= 1; alpha += Time.deltaTime * 2)
		{
			Color bgC = Background.color;
			bgC.a = alpha / 2;
			Background.color = bgC;
			Color frC = Frame.color;
			frC.a = alpha;
			Frame.color = frC;
			Color txC = Text.color;
			txC.a = alpha;
			Text.color = txC;
			yield return null;
		}

		StartCoroutine("Wait");
	}
	IEnumerator Wait()
	{
		yield return new WaitForSeconds(WaitingSeconds);
		StartCoroutine("FadeOut");
	}
	IEnumerator FadeOut()
	{
		
		for (float alpha = 1f; alpha >= 0; alpha -= Time.deltaTime*2)
		{
			Color bgC = Background.color;
			bgC.a = alpha / 2;
			Background.color = bgC;
			Color frC = Frame.color;
			frC.a = alpha;
			Frame.color = frC;
			Color txC = Text.color;
			txC.a = alpha;
			Text.color = txC;
			yield return null;
		}
		Destroy(gameObject);
	}

	public static void SystemMsg(string msg, DesMode mode, float seconds = 3.0f)
	{
		GameObject obj = Instantiate(Resources.Load("Prefab/SystemMsg", typeof(GameObject))) as GameObject;
		SysMsgCtrl ctrl = obj.GetComponent<SysMsgCtrl>();
		ctrl.Message = msg;
		ctrl.DestroyMode = mode;
		ctrl.WaitingSeconds = seconds;

	}
}
