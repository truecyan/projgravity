﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PhysicalObject : MonoBehaviour
{
	private bool _freeze = false;
	
	[HideInInspector] public GameObject[] Planets;
	[HideInInspector] public Rigidbody2D Rigidbody;
	[HideInInspector] public SpriteRenderer Renderer;
	
	[Header("Object Properties")]
	public float Mass = 1f;
	public bool Controllable;
	public bool Simulate = true;
	public float ExitOffset = 0.5f;

	[Header("Object State")]
	public GameObject CurrPlanet;
	public bool OnPlanet;
	public bool OnBox;
	public bool OnBoxSide;
	public Vector2 GravityForce;
	public float GravityForceMag;
	public float Speed;

	private Vector3 _savedPos;
	private Quaternion _savedRot;
	private Vector2 _savedVel;
	private Vector2 _prevVel;
	//private float _colDist;

	public static UnityEvent OnCannonLoaded = new UnityEvent();

	// Use this for initialization
	void Start ()
	{
		//맵에 존재하는 모든 행성 받아오기
		Planets = GameObject.FindGameObjectsWithTag("Planet");
		Renderer = gameObject.GetComponent<SpriteRenderer>();
		Rigidbody = gameObject.GetComponent<Rigidbody2D>();
		Rigidbody.mass = Mass;
		OnCannonLoaded.AddListener(FreezeStart);
		CannonCtrl.DoLaunch.AddListener(FreezeEnd);
		CheckPointCtrl.OnSave.AddListener(SaveStatus);
		PlayerMovement.OnReload.AddListener(ApplyStatus);
	}

	private void OnTriggerStay2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("PlanetCollider") && other.isTrigger)
		{
			Rigidbody.AddForce(-Rigidbody.velocity * GameManager.Instance.WaterResistConstant);
		}
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag("PlanetCollider"))
		{
			var planet = other.transform.parent.gameObject;
			if (TryLanding(planet) && gameObject.CompareTag("Player"))
			{
				gameObject.GetComponent<PlayerMovement>().LandingDamage();
			}

			if (CurrPlanet == planet)
			{
				OnPlanet = true;
			}
		}
		else if(other.gameObject.CompareTag("Box"))
		{
			if (gameObject.CompareTag("Player"))
			{
				if(SideCheck(other)) return;
			}
			GameObject planet = other.gameObject.GetComponent<PhysicalObject>().CurrPlanet; //상자가 닿아있는 행성 받아오기
			if (planet != null)
			{
				Debug.Log("Check");
				OnBox = true;
				if (TryLanding(planet) && gameObject.CompareTag("Player"))
				{
					gameObject.GetComponent<PlayerMovement>().LandingDamage();
				}
			}
		}

		if (IsLanding() && gameObject.CompareTag("Player"))
		{
			//Debug.Log("Reset");
			gameObject.GetComponent<PlayerMovement>().LandingReset();
		}
	}

	private void OnCollisionStay2D(Collision2D other)
	{
		if (CurrPlanet != null && !LandingCheck(0))
			Exit();
		if (other.gameObject.CompareTag("Box"))
		{
			if (gameObject.CompareTag("Player"))
			{
				if (!SideCheck(other))
				{
					//옆면 접촉이 아니면 상자 위에 있는 것
					OnBox = true;
					OnBoxSide = false;
				}
			}
		}
	}

	private void OnCollisionExit2D(Collision2D other)
	{
		OnBox = false;
		OnBoxSide = false;
	}
	void FixedUpdate () {
		if (_freeze) return;
		//중력 계산
		Vector2 totalForce = Vector2.zero;
		foreach (var planet in Planets)
		{
			Vector2 pos = planet.transform.position;
			var planetInfo = planet.GetComponent<AsteroidProperties>();
			float planetMass = planetInfo.Mass;
			float planetSize = planetInfo.Size;
			Vector2 distVector = pos - Rigidbody.position;
			if (planetInfo.PlanetType == AsteroidProperties.Type.Water)
			{
				var distance = (planet.transform.position - transform.position).magnitude;
				if (distance < planetSize / 2f)
				{
					planetMass = planetMass * Mathf.Pow(distance / (planetSize / 2),3);
					if (planetMass < 0.01f) continue;
				}
			}
			totalForce += GameManager.Instance.GravityConstant*distVector.normalized*planetMass*Mass/Sqr(distVector.magnitude);
		}
		//중력 적용
		GravityForce = totalForce;
		GravityForceMag = GravityForce.magnitude;
		Speed = Rigidbody.velocity.magnitude;

		if (Simulate)
			Rigidbody.AddForce(totalForce);
		else
			Rigidbody.velocity = Vector2.zero;
	}
	private void OnDestroy()
	{
		OnCannonLoaded.RemoveListener(FreezeStart);
		CannonCtrl.DoLaunch.RemoveListener(FreezeEnd);
		CheckPointCtrl.OnSave.RemoveListener(SaveStatus);
	}
	public void Land()
	{
		//Debug.Log("Land");
		Vector3 normalVector = CurrPlanet.transform.position - transform.position;
		float nowAngle = Mathf.Atan2(-transform.up.y, -transform.up.x) * Mathf.Rad2Deg;
		float toAngle = Mathf.Atan2(normalVector.y, normalVector.x) * Mathf.Rad2Deg;
		float rot = toAngle - nowAngle;

		transform.Rotate(0f,0f,rot);
	}
	public void Exit()
	{
		Debug.Log("Exit");
		Vector2 planetVelocity = CurrPlanet.GetComponent<AsteroidProperties>().GetVelocity();
		Debug.Log(planetVelocity.magnitude);
		Rigidbody.velocity += planetVelocity;
		CurrPlanet = null;
		transform.parent = null;
		OnPlanet = false;
		OnBox = false;
		OnBoxSide = false;
	}
	public void Move(float amount)
	{
		if(OnPlanet || OnBox)
		{
			var pos = transform.position;
			Vector2 r = (pos - CurrPlanet.transform.position).normalized; 	//방사방향 벡터
			Vector2 t = Vector3.Cross(r, Vector3.forward).normalized;		//접선방향 벡터
			var normalVel = ProjectionMag(Rigidbody.velocity, r);
			Rigidbody.velocity = amount * t + normalVel * r;
			Land();
		}
	}
	private bool LandingCheck(float offset)
	{
		var pos = transform.position;
		AsteroidProperties planet = CurrPlanet.GetComponent<AsteroidProperties>();
		Vector2 r = (pos - CurrPlanet.transform.position).normalized; 	//방사방향 벡터
		Vector2 t = Vector3.Cross(r, Vector3.forward).normalized;		//접선방향 벡터

		return Mass * Sqr(Mathf.Abs(ProjectionMag(Rigidbody.velocity, t))) /
		       (planet.Size / 2) //원심력
		       + ProjectionMag(GravityForce, r) //방사방향 중력
		       < offset; //중력이 더 강할 경우
	}
	private bool TryLanding(GameObject other)
	{
		if (CompareTag("Player"))
		{
			var movement = GetComponent<PlayerMovement>();
			if (movement.stateJumpedPlanet == other && !movement.stateFalling)
			{
				Debug.Log("Landing Skip");
				return false;
			}
		}
		CurrPlanet = other.gameObject;
		//Debug.Log("Planet Set : "+other.gameObject);
		if (LandingCheck(0))
		{
			transform.parent = CurrPlanet.transform;
			return true;
		}
		CurrPlanet = null;
		return false;
	}

	private bool SideCheck(Collision2D other)
	{
		var pos = transform.position;
		//옆면에 부딛혔는지 판정하는 레이 생성 (발끝에서 조금 위 지점에서 아래, 양 옆으로 발사)
		var up = transform.up;
		var ri = transform.right;
		RaycastHit2D left = Physics2D.Raycast(pos-up*1.3f, -ri, Mathf.Infinity, LayerMask.GetMask("Planet", "Box"));
		RaycastHit2D right = Physics2D.Raycast(pos-up*1.3f, ri, Mathf.Infinity, LayerMask.GetMask("Planet", "Box"));
		RaycastHit2D down = Physics2D.Raycast(pos-up*1.3f, -up, Mathf.Infinity, LayerMask.GetMask("Planet", "Box"));

		if (left.collider == other.collider || right.collider == other.collider) //양 옆 방향 레이중 둘중 하나가 현재 부딛힌 상자와 충돌했을 경우
		{
			if (down.collider != other.collider) //아래 방향의 레이가 현재 부딛힌 상자와 충돌하지 않았을 경우
			{
				//옆면 착지 판정 - 양 옆 방향으로 발사한 레이중 하나가 현재 부딛힌 상자와 충돌하면서 아래방향 레이가 현재 부딛힌 상자와 충돌하지 않았을 경우
				OnBoxSide = true;
				return true;
			}
		}

		return false;
	}

	public bool IsLanding()
	{
		return CurrPlanet != null;
	}
	public bool IsLandingOnPlanet()
	{
		return OnPlanet;
	}
	private void FreezeStart()
	{
		_prevVel = Rigidbody.velocity;
		Rigidbody.velocity = Vector2.zero;
		_freeze = true;
	}

	private void FreezeEnd()
	{
		Rigidbody.velocity = _prevVel;
		_freeze = false;
	}

	private void SaveStatus()
	{
		_savedPos = transform.position;
		_savedRot = transform.rotation;
		_savedVel = Rigidbody.velocity;
	}
	private void ApplyStatus()
	{
		transform.position = _savedPos;
		transform.rotation = _savedRot;
		Rigidbody.velocity = _savedVel;
	}
	float Sqr(float a)
	{
		return a * a;
	}
	float ProjectionMag(Vector2 a, Vector2 to)
	{
		return Vector2.Dot(a, to) / Vector2.Dot(to, to) * to.magnitude;
	}
}
