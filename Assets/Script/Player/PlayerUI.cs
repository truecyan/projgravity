﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{

	[HideInInspector] public PlayerProperties PlayerInfo;

	public Image[] HPFill = new Image[10];
	public int HPCount = -1;
	public Image[] BatFill = new Image[2];
	public int BatCount = -1;
	public Text FuelText;
	public int FuelAmount;
	private float HPFillAmount;
	
	// Use this for initialization
	void Start ()
	{
		PlayerInfo = gameObject.GetComponent<PlayerProperties>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (HPCount != PlayerInfo.HPCurr)
		{
			HPCount = PlayerInfo.HPCurr;
			for (int i = 0; i < HPCount; i++)
			{
				HPFill[i].enabled = true;
			}
			for (int i = Mathf.Max(HPCount, 0); i < 10; i++)
			{
				HPFill[i].enabled = false;
			}
		}
		if (BatCount != PlayerInfo.AirCurr)
		{
			BatCount = PlayerInfo.AirCurr;
			for (int i = 0; i < BatCount; i++)
			{
				BatFill[i].enabled = true;
			}
			for (int i = BatCount; i < 1; i++)
			{
				BatFill[i].enabled = false;
			}
		}
		FuelAmount = Mathf.RoundToInt(PlayerInfo.FuelCurr);
		FuelText.text = FuelAmount + "%";
	}
}
