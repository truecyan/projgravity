﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public float GravityConstant = 3f;
	public float WaterResistConstant = 0.1f;
	public float PlayerFriction = 0.4f;

	public bool InStage;		//스테이지 내부일 경우 true
	public int WorldNum = -1;	//월드 번호, 스테이지 내부가 아닐 경우 -1
	public int StageNum = -1;	//스테이지 번호, 스테이지 내부가 아닐 경우 -1

	public readonly string[] WorldName =
	{
		"Pisces", "Aquarius", "Capricorn", "Sagittarius", "Scorpius", "Libra", "Virgo", "Leo", "Cancer", "Gemini",
		"Taurus", "Aries"
	}; //월드 이름

	public readonly string[] StageName =
		{"Alpha", "Beta", "Gamma", "Delta", "Epsilon", "Zeta", "Eta", "Theta", "Iota", "Kappa"}; //스테이지 이름

	public int[,] StageData = new int[12,10];

	private static GameManager _instance = null;

	public static GameManager Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = FindObjectOfType<GameManager>();
				if (_instance == null)
				{
					var Object = Instantiate(Resources.Load<GameObject>("_GameManager/GameManager"));
					_instance = Object.GetComponent<GameManager>();
				}
			}

			return _instance;
		}
	}

	// Use this for initialization
	void Awake ()
	{
		if (_instance == null)
			_instance = this;
		else if (_instance != this)
			Destroy(gameObject);
		for (int i = 0; i < 3; i++)
		{
			StageData[i, 0] = 1;
			StageData[i, 1] = 1;
		}

		StageData[2, 1] = 0;
		DontDestroyOnLoad(gameObject);
	}
	public void StageStart(int world,int stage)
	{
		if (StageData[world, stage] == 0) return;
		InStage = true;
		StageNum = stage;
		WorldNum = world;
		Initiate.Fade(WorldName[WorldNum]+StageName[StageNum], Color.black, 2.0f);
	}
	public void StageExit(int result)
	{
		StageData[WorldNum,StageNum] = result;
		InStage = false;
		var color = Color.black;
		if (result > 1) color = Color.white;
		Initiate.Fade("StageSelect", color, 2.0f);
	}
	// Update is called once per frame
	void Update () {
		
	}
}
