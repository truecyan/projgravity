﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{

	[HideInInspector] public GameObject Player;
	[HideInInspector] public GameObject Camera;
	public static GameObject Cannon;
	public float FollowTime = 0.5f;
	public float RotationTime = 0.5f;
	private bool _followPlayer = true;
	private Quaternion _rotation = Quaternion.identity;
	private Vector3 _currVel;
	private Vector3 _currRotVel;

	
	// Use this for initialization
	void Start ()
	{
		Camera = gameObject;
		Player = GameObject.FindGameObjectWithTag("Player");
		ObjectCtrl.OnCannonLoaded.AddListener(CameraToCannon);
		CannonCtrl.DoLaunch.AddListener(CameraToPlayer);
	}

	// Update is called once per frame
	void FixedUpdate ()
	{
		if (_followPlayer)
		{
			transform.position =
				Vector3.SmoothDamp(transform.position, Player.transform.position-Vector3.forward*10, ref _currVel, FollowTime);
			var angle = _rotation.eulerAngles.z - transform.rotation.eulerAngles.z;
			while (angle > 180) angle -= 360;
			while (angle < -180) angle += 360;
			var rot = Vector3.SmoothDamp(Vector3.zero, new Vector3(0,0,angle), ref _currRotVel, RotationTime);
			transform.Rotate(rot);
		}
	}

	private void OnDestroy()
	{
		ObjectCtrl.OnCannonLoaded.RemoveListener(CameraToCannon);
		CannonCtrl.DoLaunch.RemoveListener(CameraToPlayer);
	}

	public void RotateCamera(Quaternion rot)
	{
		_rotation = rot;
	}
	void CameraToCannon()
	{
		if (Cannon == null)
		{
			Debug.LogError("There's No Cannon to Fix Camera!");
			return;
		}

		_followPlayer = false;
		transform.parent = Cannon.transform.GetChild(0);
		transform.localPosition = new Vector3(0,14f,-10);
		transform.localRotation = Quaternion.identity;
	}

	void CameraToPlayer()
	{
		_followPlayer = true;
		transform.parent = Player.transform;
		transform.localPosition = new Vector3(0,0,-10);
		transform.localRotation = Quaternion.identity;
	}
}
