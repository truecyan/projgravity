﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Object = System.Object;

public class PlayerMovement : MonoBehaviour
{
	
	[HideInInspector] public GameObject[] Planets; //맵에 존재하는 모든 행성을 담는 배열
	[HideInInspector] public Rigidbody2D Rigidbody; //플레이어 오브젝트의 Rigidbody 컴포넨트
	[HideInInspector] public PlayerProperties PlayerInfo; //플레이어 체력,추진게이지 등의 정보를 담고 있음
	[HideInInspector] public CameraMovement Camera; //카메라의 정보를 담고 있음
	[HideInInspector] public PhysicalObject Physics;
	
	[SerializeField] private PlayerAnimation Animation; // 플레이어 애니메이션 처리하는 컴포넌트
	
	[Header("Environment Constants")]//플레이어의 주변 환경에 대한 상수
	public float rotConstant = 5.0f; 		//공중에서 중력 방향으로 회전하는 각속도
	public int fallDamageConstant = 2;		//잘못 착지했을 때의 데미지 (착지하며 머리, 다리 콜라이더 모두 충돌할 경우)
	public int headFallDamageConstant = 4;	//머리로 착지했을 때의 데미지
	public int healMachineConstant = 1; //초당 회복량
	public int spineDamageConstant = 1; //회당 데미지
	
	[Header("Player Movement Constants")]//플레이어의 이동과 관련된 상수
	public float jumpConstant = 18.0f;			//점프 세기 //공중 점프시에도 같은 세기 적용
	public float longJumpTimeInterval = 0.3f;	//점프 후 롱점프 가능한 시간
	public float longJumpRelativeImpulse = 1f;	//상대적 롱점프 세기(점프 세기 대비)
	public float propelConstant = 7.0f;			//추진 세기
	public float fuelConsumeConstant = 0.015f;	//추진시 연료 소모 정도
	public float grabVelocityConstant = 0.8f;	//상자를 잡을 시 속도 감소 정도
	public float headFallStunTimeConstant = 1f;
	public float fallStunTimeConstant = 0.5f;

	[Header("State Variables")]//플레이어 상태 변수
	public GameObject stateJumpedPlanet;//점프 한 행성을 담는 변수 (매우 짧은 거리 동안만 보존)
	public GameObject stateGrabbedBox;	//잡은 상자를 담는 변수
	public bool stateFreeze;		//대포가 장전되어 모든 오브젝트가 멈춰야 할때 True
	public bool stateStun;			//착지를 잘못하여 카메라 회전을 기다리는 동안 True
	public bool stateFalling;		//추락 중 True
	public bool stateJumping;		//지상에서 점프 한 경우에만 점프 중 True
	public bool statePropel;		//추진 중 True
	public bool stateDash;			//대시 중 True
	public bool stateFloorDash;		//바닥 대시 중 True
	public bool stateDashLast;		//대시 중 연료 부족으로 마지막 추진 중 True
	public bool stateLeft;			//왼쪽을 보고 있을 때 True
	public bool stateGrab;			//상자를 잡고 있을 때 True
	public bool stateGetCore;		//스테이지 끝에 있는 에너지 코어 획득 시 True
	public bool stateStageClear;	//에너지 코어 획득 후 죽지 않고 바닥에 착지시 True
	public bool stateDead;			//죽었을 경우 True

	//Legacy
	[Header("Old State Variables")]
	public bool stateAirJump;		//공중 점프 시 True
	public bool stateOnTrampoline;	//트램폴린 트리거와 충돌 해 있을 시 True
	public float stateTramForce;	//충돌 하고 있는 트램폴린이 튕겨내는 힘을 담는 변수


	[Header("Movement Variables")]
	public Vector3 gravityForce;	//현재 위치의 중력 벡터를 담는 변수, 충돌 상황에서 방향을 고정하는데 사용
	public float gravityForceMag;
	public float playerSpeed;
	public Vector2 dashMoveVector;

	[Header("Collision Checker")]
	public CollisionCheck HeadCol;
	public CollisionCheck FeetCol;
	public CollisionCheck HandCol;

	//내부 변수 (인스펙터 표시 x)
	private Vector2 _prevVel;
	private Vector3 _savedPos;
	private Quaternion _savedRot;
	private Vector2 _savedVel;
	private int _savedHP;

	private float _jumpTime = 0;

	public static UnityEvent OnReload = new UnityEvent();

	
	//----------------------------------------------------------------------------------------------------------------//
	void Start () {
		//플레이어 속성 받아오기
		PlayerInfo = gameObject.GetComponent<PlayerProperties>();
		
		//맵에 존재하는 모든 행성 받아오기
		Planets = GameObject.FindGameObjectsWithTag("Planet");

		//컴포넌트 받아오기
		Physics = gameObject.GetComponent<PhysicalObject>();
		Rigidbody = gameObject.GetComponent<Rigidbody2D>();
		Rigidbody.mass = Physics.Mass; //속성 적용
		Camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraMovement>();


		//이벤트 연결
		ObjectCtrl.OnCannonLoaded.AddListener(FreezeStart);
		CannonCtrl.DoLaunch.AddListener(FreezeEnd);
		//CannonCtrl.DoLaunch.AddListener(ForceExit);
		CheckPointCtrl.OnSave.AddListener(SaveStatus);
		OnReload.AddListener(ApplyStatus);
	}
	//----------------------------------------------------------------------------------------------------------------//
	private void OnTriggerEnter2D(Collider2D other)
	{
		//표지판 접근시
		if (other.gameObject.CompareTag("Sign"))
		{
			other.gameObject.GetComponent<SignCtrl>().Activate();
		}
		//종료 지점 도착시
		else if (other.gameObject.CompareTag("EndPoint"))
		{
			Destroy(other.gameObject.GetComponent<Collider2D>());
			other.gameObject.transform.parent = transform;
			stateGetCore = true;
		}
		else if (other.gameObject.CompareTag("CheckPoint") && Physics.IsLanding())
		{
			if (!other.GetComponent<CheckPointCtrl>().Active)
			{
				SysMsgCtrl.SystemMsg("체크포인트",SysMsgCtrl.DesMode.AfterSeconds);
				other.gameObject.GetComponent<CheckPointCtrl>().Checkpoint();
			}
		}
		else if (other.gameObject.CompareTag("Trampoline"))
		{
			if (!Physics.IsLanding())
			{
				stateOnTrampoline = true;
				TrampolineCtrl tram = other.gameObject.GetComponent<TrampolineCtrl>();
				tram.Activate();
				stateTramForce = tram.BounceForce;
			}
		}
		else if (other.gameObject.CompareTag("Starlight"))
		{
			Destroy(other.gameObject);
		}
		else if (other.gameObject.CompareTag("CameraMarker"))
		{
			Camera.RotateCamera(other.transform.rotation);
		}
	}
	//----------------------------------------------------------------------------------------------------------------//
	private void OnTriggerStay2D(Collider2D other)
	{
		if (stateFreeze) return;
		if (other.gameObject.CompareTag("RepairKit"))
		{
			if (Input.GetButton("Access"))
			{
				Heal(PlayerInfo.HPMax);
				Destroy(other.gameObject);
			}
		}
		else if(other.gameObject.CompareTag("Fuel"))
		{
			if (Input.GetButton("Access") && PlayerInfo.FuelCurr < PlayerInfo.FuelMax)
			{
				var fuel = other.GetComponent<FuelProperties>();
				PlayerInfo.FuelCurr += fuel.Amount;
				if (PlayerInfo.FuelCurr > PlayerInfo.FuelMax)
					PlayerInfo.FuelCurr = PlayerInfo.FuelMax;
				fuel.Amount = 0;
				Destroy(other.gameObject);
			}
		}
		else if (other.gameObject.CompareTag("HealMachine"))
		{
			Heal(healMachineConstant);
		}
		else if (other.gameObject.CompareTag("Spine"))
		{
			Damage(spineDamageConstant);
		}
	}
	//----------------------------------------------------------------------------------------------------------------//
	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Sign"))
		{
			other.gameObject.GetComponent<SignCtrl>().Deactivate();
		}
		else if (other.gameObject.CompareTag("Trampoline"))
		{
			other.gameObject.GetComponent<TrampolineCtrl>().Deactivate();
			stateOnTrampoline = false;
		}
	}
	//----------------------------------------------------------------------------------------------------------------//
	private void OnCollisionEnter2D(Collision2D other)
	{

		var pos = transform.position;

		if (other.gameObject.CompareTag("PlanetCollider"))
		{
			if (stateOnTrampoline) //트램폴린 트리거 위에 있을시
			{
				Bounce(stateTramForce); //튕김
			}
		}
	}
	//----------------------------------------------------------------------------------------------------------------//
	void FixedUpdate() //초당 일정한 횟수 실행을 목표로 하기 때문에 한 프레임에 여러번 실행될 수 있다는 점에 유의할것!!!!
	{
		if (stateFreeze) return;

		//이동 전반----------------------------------------------------------------------------------------------------//
		if (!stateDead && !stateStun && !stateGetCore)
		{
			//Debug.Log(Physics);
			if (Physics.IsLanding())
			{
				//가로이동(지상)
				var moveSpeed = PlayerInfo.MoveSpeed;
				if (stateFloorDash)
					moveSpeed = PlayerInfo.DashSpeed;
				Physics.Move(Input.GetAxis("Horizontal") * moveSpeed);
			}
			else if(!(stateDash||stateDashLast))
			{
				//공중 기울임
				transform.Rotate(0,0,-PlayerInfo.LeanConstant*Input.GetAxis("Horizontal"));
			}

			bool backStep = false; 
			
			if (Input.GetAxis("Horizontal") < 0)
			{
				if (!stateGrab)
					stateLeft = true;
				else if (!stateLeft)
					backStep = true;
			}
			else if (Input.GetAxis("Horizontal") > 0)
			{
				if (!stateGrab)
					stateLeft = false;
				else if (stateLeft)
					backStep = true;
			}
			var pos = HandCol.transform.localPosition;
			HandCol.transform.localPosition = new Vector3((stateLeft?-1f:1f) * Mathf.Abs(pos.x),pos.y,pos.z);
			Animation.Flip(stateLeft);
		
			//애니메이션 적용 - 걷기
			if (Physics.IsLanding())
			{
				float magnitude = Mathf.Abs(Input.GetAxis("Horizontal"));
				if (magnitude >= 0.001f)
				{
					if(!stateGrab)
						Animation.Run(magnitude * (stateFloorDash? 1.5f: 1f));
					else
					{
						if(!backStep)
							Animation.GrabWalkFront(magnitude * grabVelocityConstant);
						else
							Animation.GrabWalkBack(magnitude * grabVelocityConstant);
					}
				}
				else
				{
					if(!stateGrab)
						Animation.Idle();
					else
						Animation.Grab();
				}
			}
			else
			{
				Animation.Jump(stateGrab);
			}
		}
		//플레이어 기준 벡터 (앞, 아래)
		Vector3 fwdVector = transform.right;
		Vector3 dwnVector = -transform.up;
		
		//추락 판정 : 플레이어의 발쪽으로 떨어질것, 착지해있지 않을것, 추진중이지 않을 것
		if (Vector2.Dot(Convert3Dto2D(dwnVector), Rigidbody.velocity) > 0 && !Physics.IsLanding() && !statePropel)
		{
			stateFalling = true;
			stateJumping = false;
			stateAirJump = false;
			Animation.Fall(stateGrab);
		}
		else
			stateFalling = false;

		//과도한 회전을 막기 위한 벡터(초기, z값)
		if (!Physics.IsLanding())
		{
			float initCross = Vector3.Cross(dwnVector, Physics.GravityForce).z;

			//플레이어 회전
			float rot = Vector2.Dot(Physics.GravityForce, fwdVector)*rotConstant*Time.deltaTime;

			if(!(stateDash))
				transform.Rotate(0f,0f,rot);

			//과도하게 회전할 시 중력 방향으로 플레이어의 회전방향을 맞춤
			dwnVector = -gameObject.transform.up;
			float newCross = Vector3.Cross(dwnVector, Physics.GravityForce).z;
			if (initCross * newCross < 0)
			{
				float nowAngle = Mathf.Atan2(dwnVector.y, dwnVector.x) * Mathf.Rad2Deg;
				float toAngle = Mathf.Atan2(Physics.GravityForce.y, Physics.GravityForce.x) * Mathf.Rad2Deg;
				rot = toAngle - nowAngle;
				transform.Rotate(0f,0f,rot);
			}
		}
	}
	//----------------------------------------------------------------------------------------------------------------//
	private void Update()
	{
		//점프
		if (stateFreeze) return;
		
		if (stateDead) return;

		if (!stateDead && stateGetCore && Physics.IsLanding() && !stateStageClear)
		{
			GetComponentInChildren<EndPointCtrl>().StageEnd();
			stateStageClear = true;
		}

		if (Input.GetButtonDown("Jump"))
		{
			if (Physics.IsLanding())
				Jump();
			else if (PlayerInfo.FuelCurr > 0)
			{
				statePropel = true;
			}
		}
		if (Input.GetButton("Jump"))
		{
			if (statePropel)
			{
				if (PlayerInfo.FuelCurr > 0)
					Propel();
				else
				{
					statePropel = false;
					stateJumping = true;
					Animation.Jump(stateGrab);
				}

			}
			else if (stateJumping)
				LongJump();
		}
		if (Input.GetButtonUp("Jump"))
		{
			_jumpTime = 0;		//점프 키 뗀 뒤 롱점프 방지
			
			if (statePropel)
			{
				statePropel = false;
				stateJumping = true;
				Animation.Jump(stateGrab);
			}
		}
		if (Input.GetButtonDown("Dash"))
		{
			if (Physics.IsLanding())
			{
				stateFloorDash = true;
			}
		}
		if (Input.GetButton("Dash"))
		{
			if (Physics.IsLanding())
			{
				stateFloorDash = true;
				return;
			}

		}
		if (Input.GetButtonUp("Dash"))
		{
			if (stateFloorDash)
			{
				stateFloorDash = false;
				stateJumping = true;
				Animation.Jump(stateGrab);
			}
		}

		if (Input.GetButtonDown("Grab"))
		{
			if (HandCol.ColCheck())
			{
				GameObject box = HandCol.ColBox();
				if (box != null)
				{
					stateGrab = true;
					stateGrabbedBox = box;
					box.transform.parent = transform;
				}
			}
		}
		if (Input.GetButtonUp("Grab"))
		{
			stateGrab = false;
			if(stateGrabbedBox != null)
				stateGrabbedBox.transform.parent = null;
			stateGrabbedBox = null;
		}
		if (Input.GetButtonDown("Restart"))
		{
			Die();
		}

		if (Input.GetButtonDown("Cancel"))
		{
			GameManager.Instance.StageExit(1);
		}
	}
	//----------------------------------------------------------------------------------------------------------------//
	void OnDestroy()
	{
		ObjectCtrl.OnCannonLoaded.RemoveListener(FreezeStart);
		CannonCtrl.DoLaunch.RemoveListener(FreezeEnd);
		//CannonCtrl.DoLaunch.RemoveListener(ForceExit);
		CheckPointCtrl.OnSave.RemoveListener(SaveStatus);
		OnReload.RemoveListener(ApplyStatus);
	}
	//----------------------------------------------------------------------------------------------------------------//
	private void Jump()
	{
		//점프 한 위치, 행성 저장
		if (stateStun) return;
		stateJumpedPlanet = Physics.CurrPlanet;
		Debug.Log("Jumped / " + (Time.time - _jumpTime) + " / " + stateJumpedPlanet);
		var moveSpeed = PlayerInfo.MoveSpeed;
		if (stateFloorDash)
			moveSpeed = PlayerInfo.DashSpeed;
		Physics.Move(Input.GetAxis("Horizontal") * moveSpeed);
		Physics.Exit();
		stateJumping = true;
		_jumpTime = Time.time;
		//Vector2 planetSpeed = stateJumpedPlanet.GetComponent<AsteroidProperties>().GetVelocity();
		//Rigidbody.velocity += planetSpeed;
		Rigidbody.AddForce(gameObject.transform.up*jumpConstant, ForceMode2D.Impulse);
		
		//점프 애니메이션 실행
		Animation.Jump(stateGrab);
	}
	//----------------------------------------------------------------------------------------------------------------//
	private void LongJump()
	{
		float elapsedTime = Time.time - _jumpTime;
		if (elapsedTime > longJumpTimeInterval)
			return;
		float progress = elapsedTime / longJumpTimeInterval;

		//롱점프 힘 선형적으로 감소, 총 임펄스는 jumpConstant*longJumpRelativeImpulse와 같음
		Rigidbody.AddForce(transform.up*jumpConstant*longJumpRelativeImpulse*2*(1-progress)/longJumpTimeInterval, ForceMode2D.Force);
	}
	//----------------------------------------------------------------------------------------------------------------//
	private void Bounce(float force)
	{
		//점프 한 위치, 행성 저장
		if (Physics.CurrPlanet == null) return;
		stateJumpedPlanet = Physics.CurrPlanet;
		
		Rigidbody.AddForce(gameObject.transform.up*force, ForceMode2D.Impulse);
		
		//점프 애니메이션 실행
		Animation.Jump(stateGrab);

		//레이어 충돌 무시 설정 켜기
		if(Physics.OnPlanet)
			Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"),LayerMask.NameToLayer("Planet"),true);
		else if(Physics.OnBox)
			Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"),LayerMask.NameToLayer("Box"),true);
		else
			Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"),LayerMask.NameToLayer("Wall"),true);
	}
	//----------------------------------------------------------------------------------------------------------------//
	private void Propel()
	{
		if (PlayerInfo.FuelCurr > 0)
		{
			float spdUp = Vector2.Dot(Rigidbody.velocity,transform.up);
			if (spdUp < 1f) spdUp = 1f;

			if (PlayerInfo.FuelCurr > propelConstant * fuelConsumeConstant * Physics.GravityForceMag * Time.deltaTime)
			{
				PlayerInfo.FuelCurr -= propelConstant * fuelConsumeConstant * Physics.GravityForceMag * Time.deltaTime;
				Debug.Log("Force : " + propelConstant +" / "+ Physics.GravityForceMag +" / "+ spdUp);
                Rigidbody.AddForce(transform.up * propelConstant * Physics.GravityForceMag / spdUp, ForceMode2D.Force);
			}
			else
			{
				PlayerInfo.FuelCurr = 0;
				Rigidbody.AddForce(transform.up * PlayerInfo.FuelCurr / (fuelConsumeConstant * Time.deltaTime) / spdUp, ForceMode2D.Force);
			}
			stateJumping = false;
			Animation.Propel();
		}
		else
		{
			Debug.LogError("Error! Can't propel if FuelCurr is zero.");
		}
	}
	//----------------------------------------------------------------------------------------------------------------//
	private void FreezeStart()
	{
		_prevVel = Rigidbody.velocity;
		Rigidbody.velocity = Vector2.zero;
		stateFreeze = true;
		Animation.SetEnable(false);
	}
	//----------------------------------------------------------------------------------------------------------------//
	private void FreezeEnd()
	{
		Rigidbody.velocity = _prevVel;
		stateFreeze = false;
		Animation.SetEnable(true);
	}
	//----------------------------------------------------------------------------------------------------------------//
	private void SaveStatus()
	{
		_savedPos = transform.position;
		_savedRot = transform.rotation;
		_savedVel = Rigidbody.velocity;
		_savedHP = PlayerInfo.HPCurr;
	}
	//----------------------------------------------------------------------------------------------------------------//
	private void ApplyStatus()
	{
		transform.position = _savedPos;
		transform.rotation = _savedRot;
		Rigidbody.velocity = _savedVel;
		PlayerInfo.HPCurr = _savedHP;
		Animation.Idle();
	}
	//----------------------------------------------------------------------------------------------------------------//
	public static void ApplyReload()
	{
		OnReload.Invoke();
	}
	//----------------------------------------------------------------------------------------------------------------//
	public void LandingReset()
	{
		stateJumping = false;
		stateAirJump = false;
	}
	//----------------------------------------------------------------------------------------------------------------//
	private int CheckDamage()
	{
		if (HeadCol.ColCheck())
		{
			if (FeetCol.ColPlanet() == HeadCol.ColPlanet() || (FeetCol.ColBox() != null && HeadCol.ColBox() != null && FeetCol.ColPlanet() == null))
				return fallDamageConstant;
			if (FeetCol.ColPlanet() == null && FeetCol.ColBox() == null)
				return headFallDamageConstant;
		}
		return 0;
	}
	public void LandingDamage()
	{
		int damage = CheckDamage();
		if (damage > 0)
		{
			var stunTime = fallStunTimeConstant;
			if (damage > fallDamageConstant) stunTime = headFallStunTimeConstant;
			PlayerInfo.HPCurr -= damage;
			StartCoroutine(Stun(stunTime));
		}
	}
	IEnumerator Stun(float stunTime)
	{
		Physics.Land();
		stateStun = true;
		if (PlayerInfo.HPCurr <= 0)
		{
			Die();
		}
		else
		{
            Animation.Stun();
            yield return new WaitForSeconds(stunTime);
            stateStun = false;
            Animation.Wake();
		}
	}
	public void Damage(int damage)
	{
		if (!PlayerInfo.Invincible)
		{
			PlayerInfo.HPCurr -= damage;
			if (PlayerInfo.HPCurr <= 0)
			{
				Die();
			}
			else
				PlayerInfo.StartInvincible();
		}
	}

	public void Heal(int amount)
	{
		if (!PlayerInfo.Healed)
		{
			PlayerInfo.HPCurr += amount;
            if (PlayerInfo.HPCurr > PlayerInfo.HPMax)
            	PlayerInfo.HPCurr = PlayerInfo.HPMax;
            PlayerInfo.StartHeal();
		}
	}
	//----------------------------------------------------------------------------------------------------------------//
	public void Die()
	{
		stateDead = true;
		if (stateStun) Animation.StunDie();
		else Animation.Die();

		if (CheckPointCtrl.Saved)
		{
			if (!Initiate.IsFading())
			{
				Initiate.Reload(Color.black, 1f);
			}
		}
		else
		{
			if (!Initiate.IsFading())
			{
				Scene scene = SceneManager.GetActiveScene();
				Initiate.Fade(scene.name,Color.black, 1f);
			}
		}
	}
	//----------------------------------------------------------------------------------------------------------------//
	float Sqr(float a)
	{
		return a * a;
	}
	//----------------------------------------------------------------------------------------------------------------//
	Vector2 Convert3Dto2D(Vector3 v)
	{
		return new Vector2(v.x,v.y);
	}
}
