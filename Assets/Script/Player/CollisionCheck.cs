﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class CollisionCheck : MonoBehaviour
{
	private bool _isCollided;
	private GameObject _colBox;
	private GameObject _colPlanet;
	// Use this for initialization
	void Start () {
		
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("PlanetCollider")||other.CompareTag("Box")||other.CompareTag("Wall"))
		{
			_isCollided = true;
			if (other.CompareTag("Box"))
			{
				if (_colBox != null)
				{
					if ((other.transform.position - transform.position).magnitude <
					    (_colBox.transform.position - transform.position).magnitude)
					{
						_colBox = other.gameObject;
					}
					
				}
				else
				{
					_colBox = other.gameObject;
				}
			}

			if (other.CompareTag("PlanetCollider"))
			{
				var planet = other.transform.parent.gameObject;
				if (_colPlanet != null)
				{
					if ((planet.transform.position - transform.position).magnitude <
					    (_colPlanet.transform.position - transform.position).magnitude)
					{
						_colPlanet = planet;
					}

				}
				else
				{
					_colPlanet = planet;
				}
			}
				
		}
	}
	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.CompareTag("PlanetCollider")||other.CompareTag("Box")||other.CompareTag("Wall"))
		{
			_isCollided = false;
			if (other.CompareTag("Box"))
				_colBox = null;
			if (other.CompareTag("PlanetCollider"))
				_colPlanet = null;
			if (_colPlanet != null || _colBox != null) _isCollided = true;
		}
	}
	// Update is called once per frame
	void Update () {
		
	}

	public bool ColCheck()
	{
		return _isCollided;
	}

	public GameObject ColBox()
	{
		return _colBox;
	}
	public GameObject ColPlanet()
	{
		return _colPlanet;
	}
}
