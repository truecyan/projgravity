﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProperties : MonoBehaviour
{
	//public float PlayerMass = 1.0f;
	public float MoveSpeed = 6;
	public float DashSpeed = 12;
	public int HPMax = 10;
	public int HPCurr = 10;
	public float FuelMax = 100;
	public float FuelCurr = 100;
	public int AirCharge = 1;
	public int AirCurr = 1;
	public float LeanConstant = 0.7f;
	public bool Invincible = false;
	public float InvincibleTime = 0.5f; //피격시 무적시간 (초)
	public bool Healed = false;
	public float HealDelayTime = 0.5f;
	[HideInInspector] public float InvincibleTimeLeft = 0f;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (HPCurr < 0) HPCurr = 0;
	}

	public void StartInvincible()
	{
		Invincible = true;
		StartCoroutine("InvincibleWait");
	}

	public void StartHeal()
	{
		Healed = true;
		StartCoroutine("HealDelay");
	}

	IEnumerator InvincibleWait()
	{
		yield return new WaitForSeconds(InvincibleTime);
		Invincible = false;
	}
	IEnumerator HealDelay()
	{
		yield return new WaitForSeconds(HealDelayTime);
		Healed = false;
	}
}
