﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
public class PlayerAnimation : MonoBehaviour {

	[SerializeField] private SpriteRenderer spriteRenderer; 
	[SerializeField] private Animator animator;
	[SerializeField] private List<EffectProp> effects;
	private Type currentEffectType = Type.IDLE;
	private bool showEffect = true;

	public const string VAR_STATE = "currentAnimation";
	public const string VAR_SPEED = "runSpeed";

	[System.Serializable]
	public enum Type : int
	{
		IDLE = 0,
		JUMP = 1,
		FALL = 2,
		RUN = 3,
		BOOST = 4,
		STUN = 5,
		WAKE = 6,
		DIE = 7,
		GRAB = 8,
		GRABWALKFRONT = 9,
		GRABWALKBACK = 10,
		GRABJUMP = 11,
		GRABFALL = 12,
		GRABBOOST = 13
	}

	public void Flip(bool lookingLeft)
	{
		spriteRenderer.flipX = lookingLeft;
	}

	public void SetEnable(bool b)
	{
		animator.enabled = b;
	}

	public void Idle()
	{
		if (currentEffectType == Type.STUN)
		{
			print(123);
			return;
		}
		Transition(Type.IDLE);
	}

	public void Jump(bool isGrabbing)
	{
		Transition(!isGrabbing ? Type.JUMP : Type.GRABJUMP);
	}

	public void Fall(bool isGrabbing)
	{
		Transition(!isGrabbing ? Type.FALL : Type.GRABFALL);
	}

	public void Run(float speedMultiplier)
	{
		Transition(Type.RUN);
		animator.SetFloat(VAR_SPEED, speedMultiplier);
	}

	public void Propel()
	{
		Transition(Type.BOOST);
	}

	public void Stun()
	{
		Transition(Type.STUN);
	}
	public void StunDie()
	{
		showEffect = false;
		Transition(Type.STUN);
	}

	public void Wake()
	{
		Transition(Type.WAKE);
	}

	public void Die()
	{
		Transition(Type.DIE);
	}

	public void Grab()
	{
		Transition(Type.GRAB);
	}

	public void GrabWalkFront(float speedMultiplier)
	{
		Transition(Type.GRABWALKFRONT);
		animator.SetFloat(VAR_SPEED, speedMultiplier);
	}
	
	public void GrabWalkBack(float speedMultiplier)
	{
		Transition(Type.GRABWALKBACK);
		animator.SetFloat(VAR_SPEED, speedMultiplier);
	}
	
	private void Transition(Type t)
	{
		if (currentEffectType == t)
			return;
		currentEffectType = t;
		if(showEffect)
			ShowEffectProp(t);
		showEffect = true;
		animator.SetInteger(VAR_STATE, (int)t);
	}

	private void ShowEffectProp(Type type)
	{
		foreach (EffectProp prop in effects)
		{
			bool active = prop.type == type;
			GameObject obj = prop.effect;
			obj.SetActive(active);

			if (prop.flipAlongSprite)
			{
				Vector3 pos = obj.transform.localPosition;
				pos.x = (spriteRenderer.flipX? -1: 1) * Mathf.Abs(pos.x);
				obj.transform.localPosition = pos;
			}
		}
	}

	[System.Serializable]
	public struct EffectProp
	{
		public Type type;
		public GameObject effect;
		public bool flipAlongSprite;
	}
}