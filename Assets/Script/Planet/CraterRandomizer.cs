﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraterRandomizer : MonoBehaviour
{
	[Range(0,1)]public float[] CraterWeight;
	public GameObject CraterPrefab;
	public GameObject CraterContainer;
	public int GenerateNumber;

	// Use this for initialization
	void Start ()
	{
		CraterWeight = new float[CraterPrefab.GetComponent<CraterSpriteManager>().SpritePool.Length];
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Generate()
	{
		float sum = 0;
		foreach (var w in CraterWeight)
		{
			sum += w;
		}
		var radius = GetComponent<AsteroidProperties>().Size;
		for (int i = 0; i < GenerateNumber; i++)
		{
			var newCrater = Instantiate(CraterPrefab,CraterContainer.transform);
			var col = newCrater.GetComponent<CircleCollider2D>();

			var sprite = newCrater.GetComponent<CraterSpriteManager>();
			var maxIndex = CraterPrefab.GetComponent<CraterSpriteManager>().SpritePool.Length;
			var randomValue = sum * Random.value;
			int index;
			for (index = 0; index < maxIndex; index++)
			{
				randomValue -= CraterWeight[index];
				if (randomValue < 0) break;
			}
			if (index >= maxIndex) index = 0;
			sprite.Index = index;
			sprite.ImageSet();

			col.radius = sprite.SpriteSize[index];

			int counter = 0;
			newCrater.transform.localPosition = radius/2 * Random.insideUnitCircle;
			while (col.OverlapCollider(new ContactFilter2D(), new Collider2D[5]) >= 2)
			{
				counter++;
				newCrater.transform.localPosition = radius/2 * Random.insideUnitCircle;
				if (counter > 20)
				{
					Debug.Log("Skipped");
					DestroyImmediate(newCrater);
					break;
				}
			}
		}

		foreach (var crater in GetComponentsInChildren<CraterSpriteManager>())
		{
			var col = crater.GetComponent<Collider2D>();
			DestroyImmediate(col);
		}
	}

	public void Remove()
	{
		var craters = GetComponentsInChildren<CraterSpriteManager>();
		foreach(var crater in craters)
		{
			DestroyImmediate(crater.gameObject);
		}
	}
}
