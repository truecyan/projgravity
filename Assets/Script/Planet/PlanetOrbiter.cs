﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BansheeGz.BGSpline.Components;

public class PlanetOrbiter : MonoBehaviour {
	private BGCcCursorObjectTranslate mover;
	public float period;

	private float time = 0f;
	private float _prev;
	private AsteroidProperties _planet;
	private float _vel;

	void Start()
	{
		mover = GetComponent<BGCcCursorObjectTranslate>();
		_planet = mover.ObjectToManipulate.GetComponent<AsteroidProperties>();
	}
	private void FixedUpdate() {
		if (period <= 0)
			return;

		time += Time.fixedDeltaTime;
		if (time > period)
		{
			time -= period;
		}
		float ratio = time / period;
		mover.Cursor.DistanceRatio = ratio;
		_vel = (mover.Cursor.Distance - _prev) / Time.fixedDeltaTime;
		_planet.StdVelocity = _vel;
		_prev = mover.Cursor.Distance;
	}
}
