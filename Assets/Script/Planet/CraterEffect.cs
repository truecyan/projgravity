﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CraterEffect : MonoBehaviour
{
	private AsteroidProperties _asteroidInfo;
	private SpriteRenderer _sprite;

	// Use this for initialization
	void Start ()
	{
		_asteroidInfo = GetComponentInParent<AsteroidProperties>();
		_sprite = GetComponentInChildren<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Application.isPlaying)
			return;
		transform.localPosition = Vector3.zero;
		_sprite.transform.localPosition = Vector3.zero;
		var dwnVector = -transform.up;
		var dirVector = transform.position - _asteroidInfo.transform.position;
		float nowAngle = Mathf.Atan2(dwnVector.y, dwnVector.x) * Mathf.Rad2Deg;
		float toAngle = Mathf.Atan2(dirVector.y, dirVector.x) * Mathf.Rad2Deg;
		transform.Rotate(0f,0f,toAngle - nowAngle);

		var distConst = dirVector.magnitude / (_asteroidInfo.Size/2);

		dwnVector = -_sprite.transform.up;
		dirVector = Vector3.down;
		nowAngle = Mathf.Atan2(dwnVector.y, dwnVector.x) * Mathf.Rad2Deg;
		toAngle = Mathf.Atan2(dirVector.y, dirVector.x) * Mathf.Rad2Deg;
		_sprite.transform.Rotate(0f,0f,toAngle - nowAngle);

		if (distConst <= 1)
			transform.localScale = new Vector3(1, Mathf.Sqrt(1 - Sqr(distConst)));
		else
			Debug.LogError("Crater 오브젝트는 소행성 내부에 위치해야 합니다.");
	}

	float Sqr(float a)
	{
		return a * a;
	}
}
