﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class PlanetProperties : MonoBehaviour
{
	//행성 속성
	public enum Type
	{
		Normal,
		Water,
		Ice
	}
	public float PlanetMass = 1000.0f;
	public float PlanetSize = 10f;
	public Color PlanetColor = Color.white;
	public bool Hostile = false;
	public bool HasCore = false;
	public Type PlanetType;
	public GameObject LinePrefab;
	public PlanetProperties LinkedPlanet;
	private Line Link;
	private GameObject Arrow;

	[HideInInspector] public Collider2D Collider = new Collider2D();

	[HideInInspector] public GameObject ArrowPrefab;
	// Use this for initialization
	void Start ()
	{
		Collider = GetComponent<Collider2D>();
		ArrowPrefab = Resources.Load<GameObject>("Prefab/Arrow");
	}
	
	// Update is called once per frame
	void Update () {
		if (!Application.isPlaying)
		{
			transform.localScale = new Vector3(PlanetSize, PlanetSize, 1);
			Collider.isTrigger = (PlanetType == Type.Water);

		}
		else
		{
			if (LinkedPlanet != null)
			{
				if (Link == null)
				{
					var lineObj = Instantiate(LinePrefab);

					Link = lineObj.GetComponent<Line>();
					Link.Initialise(transform.position,LinkedPlanet.transform.position,0.04f,Color.white);

					Arrow = Instantiate(ArrowPrefab, lineObj.transform);
					Arrow.transform.localScale = Reverse(Link.transform.localScale);
				}
				else
				{
					var dir = (LinkedPlanet.transform.position - transform.position).normalized;
					Link.Initialise(transform.position + dir*PlanetSize/2,
									LinkedPlanet.transform.position - dir*LinkedPlanet.PlanetSize/2,
									0.04f,Color.white);
					if(Arrow == null) Arrow = Instantiate(ArrowPrefab, Link.transform);
					Arrow.transform.localScale = Reverse(Link.transform.localScale);
				}
			}
			else
			{
				if (Link != null)
				{
					DestroyImmediate(Link.gameObject);
					Link = null;
				}
			}
		}
	}

	Vector3 Reverse(Vector3 v)
	{
		return new Vector3(1/v.x,1/v.y,1/v.z);
	}
}
