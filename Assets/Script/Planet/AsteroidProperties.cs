﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AsteroidProperties : MonoBehaviour
{
	//행성 속성
	public enum Type
	{
		Normal,
		Water,
		Ice
	}
	public float Mass = 1000.0f;
	public float Size = 10f;
	public Color Color = Color.white;
	public bool PlanetMoving = false;
	public bool Hostile = false;
	public bool HasCore = false;
	public Type PlanetType;

	public AsteroidProperties LinkedAsteroid;
	public float StdVelocity;
	private Line Link;
	private GameObject Arrow;
	private Vector3 _prevPos;
	private float _prevMag;
	private Vector3 _velocity;
	public float VelMag;

	[HideInInspector] public Collider2D Collider = new Collider2D();

	[HideInInspector] public GameObject ArrowPrefab;
	[HideInInspector] public GameObject LinePrefab;
	[HideInInspector] public SpriteRenderer Background;

	// Use this for initialization
	void Start ()
	{
		Collider = GetComponentInChildren<Collider2D>();
		Background = GetComponentInChildren<SpriteRenderer>();
		ArrowPrefab = Resources.Load<GameObject>("Prefab/Arrow");
		LinePrefab = Resources.Load<GameObject>("Prefab/Line");
	}

	public Vector3 GetVelocity()
	{
		return _velocity;
	}

	private void FixedUpdate() {
		if (transform.position != _prevPos && PlanetMoving)
		{
			_velocity = (transform.position - _prevPos).normalized * StdVelocity;
			_velocity.z = 0f;
		}
		VelMag = _velocity.magnitude;
		_prevPos = transform.position;
		_prevMag = _velocity.magnitude;
	}

	// Update is called once per frame
	void Update () {
		if (!Application.isPlaying)
		{
			if(PlanetType == Type.Normal)
				Background.color = Color;
			Collider.transform.localScale = new Vector3(Size, Size, 1);
			Collider.isTrigger = (PlanetType == Type.Water);
		}
		else
		{
			if (LinkedAsteroid != null)
			{
				if (Link == null)
				{
					var lineObj = Instantiate(LinePrefab);

					Link = lineObj.GetComponent<Line>();
					Link.Initialise(transform.position,LinkedAsteroid.transform.position,0.04f,Color.white);

					Arrow = Instantiate(ArrowPrefab, lineObj.transform);
					Arrow.transform.localScale = Reverse(Link.transform.localScale);
				}
				else
				{
					var dir = (LinkedAsteroid.transform.position - transform.position).normalized;
					Link.Initialise(transform.position + dir*Size/2,
						LinkedAsteroid.transform.position - dir*LinkedAsteroid.Size/2,
						0.04f,Color.white);
					if(Arrow == null) Arrow = Instantiate(ArrowPrefab, Link.transform);
					Arrow.transform.localScale = Reverse(Link.transform.localScale);
				}
			}
			else
			{
				if (Link != null)
				{
					DestroyImmediate(Link.gameObject);
					Link = null;
				}
			}
		}
	}

	Vector3 Reverse(Vector3 v)
	{
		return new Vector3(1/v.x,1/v.y,1/v.z);
	}
}
