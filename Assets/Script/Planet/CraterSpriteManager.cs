﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CraterSpriteManager : MonoBehaviour
{
	public Sprite[] SpritePool = new Sprite[7];
	public float[] SpriteSize = new float[7];
	public int Index;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void ImageSet()
	{
		if (SpritePool[Index] != null)
		{
			GetComponentInChildren<SpriteRenderer>().sprite = SpritePool[Index];
		}
	}
}
