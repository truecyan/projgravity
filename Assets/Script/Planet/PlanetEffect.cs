﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetEffect : MonoBehaviour
{

	[HideInInspector] public AsteroidProperties Planet;
	[HideInInspector] public GameObject Player;
	[HideInInspector] public GameObject EndPoint;
	public GameObject CraterContainer;
	private float _wobbleMax = 0.02f;
	private float _wobbleCurr = 0;
	private float _rot = 180 * 1.618f;
	private float _period = 1;



	// Use this for initialization
	void Start ()
	{
		Player = GameObject.FindWithTag("Player");
		EndPoint = GameObject.FindWithTag("EndPoint");
		var dir = EndPoint.transform.position - transform.position;
		float nowAngle = Mathf.Atan2(-transform.right.y, -transform.right.x) * Mathf.Rad2Deg;
		float toAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		float rot = toAngle - nowAngle;

		Planet = gameObject.GetComponentInParent<AsteroidProperties>();
		if(Planet.PlanetType != AsteroidProperties.Type.Water)
			CraterContainer.transform.Rotate(0f,0f,rot);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Planet.PlanetType == AsteroidProperties.Type.Water)
		{
			var distance = (transform.position - Player.transform.position).magnitude;
			if (distance < 50f)
			{
				var curr = Mathf.Sin(_wobbleCurr) * _wobbleMax;
				if (_wobbleCurr < 2 * Mathf.PI)
				{
					_wobbleCurr += 2 * Mathf.PI / _period * Time.deltaTime;
				}
				else
					_wobbleCurr = 0;

				if(curr >= 0)
					transform.localScale = new Vector3(Planet.Size*(1+curr), Planet.Size/(1+curr), 1);
				else
				{
					transform.localScale = new Vector3(Planet.Size/(1-curr), Planet.Size*(1-curr), 1);
				}

				transform.Rotate(0, 0, _rot / Planet.Size * Time.deltaTime);
			}
		}
	}
}
