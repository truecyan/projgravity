﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadarCtrl : MonoBehaviour
{
	private GameObject[] _planets;
	private List<GameObject> _markers = new List<GameObject>();
	private GameObject _camera;
	private GameObject _player;
	private GameObject _endPoint;
	private GameObject _endMarker;
	public GameObject PlanetMarker;
	public float RadarMultiplier = 1.2f;
	public float RadarSize = 115f;

	// Use this for initialization
	void Start () {
		_planets = GameObject.FindGameObjectsWithTag("Planet");
		_camera = GameObject.FindGameObjectWithTag("MainCamera");
		_player = GameObject.FindGameObjectWithTag("Player");
		_endPoint = GameObject.FindGameObjectWithTag("EndPoint");

		foreach (GameObject planet in _planets)
		{
			AsteroidProperties planetProperty = planet.GetComponent<AsteroidProperties>();
			GameObject marker = Instantiate(PlanetMarker, gameObject.transform, true);
			_markers.Add(marker);
			marker.GetComponent<MarkerCtrl>().MarkingPlanet = planet;
			Vector3 position = _camera.transform.rotation * (planet.transform.position - _player.transform.position);
			marker.transform.localPosition = position*RadarMultiplier;
			marker.transform.localScale = new Vector3(planetProperty.Size/80f*RadarMultiplier, planetProperty.Size/80f*RadarMultiplier, 1);
			Color color = planetProperty.Color;
			color.a = 1-Mathf.Pow(position.magnitude/RadarSize,2);
			marker.GetComponent<Image>().color = color;
		}

		_endMarker = Instantiate(PlanetMarker, gameObject.transform, true);
		_endMarker.GetComponent<MarkerCtrl>().MarkingPlanet = _endPoint;
		Vector3 endPos = _camera.transform.rotation * (_endPoint.transform.position - _player.transform.position);
		_endMarker.transform.localPosition = endPos*RadarMultiplier;
		_endMarker.transform.localScale = new Vector3(0.1f, 0.1f, 1);
		_endMarker.GetComponent<Image>().color = new Color(1,1,1,1-Mathf.Pow(endPos.magnitude/RadarSize,2));
	}
	
	// Update is called once per frame
	void Update ()
	{
		foreach (GameObject marker in _markers)
		{
			GameObject planet = marker.GetComponent<MarkerCtrl>().MarkingPlanet;
			AsteroidProperties planetProperty = planet.GetComponent<AsteroidProperties>();
			Vector3 position = Quaternion.Inverse(_camera.transform.rotation) * (planet.transform.position - _player.transform.position);
			if (position.magnitude > RadarSize)
				marker.SetActive(false);
			else
			{
				if(marker.activeSelf == false)
					marker.SetActive(true);
				marker.transform.localPosition = position*RadarMultiplier;
				Color color = planetProperty.Color;
				color.a = 1-Mathf.Pow(position.magnitude/RadarSize,2);
				marker.GetComponent<Image>().color = color;
			}
		}
		Vector3 pos = Quaternion.Inverse(_camera.transform.rotation) * (_endPoint.transform.position - _player.transform.position);
		if (pos.magnitude > RadarSize)
			_endMarker.SetActive(false);
		else
		{
			if(_endMarker.activeSelf == false)
				_endMarker.SetActive(true);
			_endMarker.transform.localPosition = pos*RadarMultiplier;
			Color color = Color.white;
			color.a = 1-Mathf.Pow(pos.magnitude/RadarSize,2);
			_endMarker.GetComponent<Image>().color = color;
		}
	}
}
