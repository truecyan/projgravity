﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PlacingHelper : MonoBehaviour
{
	[HideInInspector] public GameObject[] Planets;
	[HideInInspector] public SpriteRenderer Renderer;
	public float Offset = 0f;
	
	// Use this for initialization
	void Start () {

	}

	public void Adjust()
	{
		Planets = GameObject.FindGameObjectsWithTag("Planet");
		Renderer = gameObject.GetComponent<SpriteRenderer>();
		if (Planets != null)
		{
			
			GameObject nearest = null;
			float nearestDist = -1f;
			foreach (GameObject planet in Planets)
			{
				float dist = (planet.transform.position - transform.position).magnitude;
				if (nearestDist < 0 || dist < nearestDist)
				{
					nearest = planet;
					nearestDist = dist;
				}
			}

			Vector3 normalVector = nearest.transform.position - transform.position;
			float nowAngle = Mathf.Atan2(-transform.up.y, -transform.up.x) * Mathf.Rad2Deg;
			float toAngle = Mathf.Atan2(normalVector.y, normalVector.x) * Mathf.Rad2Deg;
			float rot = toAngle - nowAngle;


			float height = transform.lossyScale.y * Renderer.sprite.bounds.size.y;
			Debug.Log(height);
			transform.Rotate(0f,0f,rot);
			transform.Translate(-Vector3.up*(normalVector.magnitude-(nearest.GetComponent<AsteroidProperties>().Size/2f+height/2f-Offset)));
		}

	}
}
