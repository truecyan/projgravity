﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarProperties : MonoBehaviour
{
	[HideInInspector] public SpriteRenderer SpriteRenderer;
	[Header("Sprite")]
	public Sprite placeholder;
	public Sprite locked;
	public Sprite notCleared;
	public Sprite cleared;
	public Sprite hiddenLocked;
	public Sprite hiddenNotCleared;
	public Sprite hiddenCleared;
	public SpriteRenderer BackEffect;
	public int state;			//0 - locked, 1 - not cleared, 2 - cleared
	public int worldNum;
	public int stageNum;
	public bool isHidden;
	public bool isStar;
	public float Period = 2;
	private float _phase;
	private float _lLimit = 0.3f;
	private float _mLimit = 1;
	
	
	// Use this for initialization
	void Start () {
		//BackEffect = GetComponentInChildren<SpriteRenderer>();
		Reset();
	}
	
	// Update is called once per frame
	void Update () {
		if (isStar)
		{
			Color c = BackEffect.color;
			switch (state)
			{
				case 1:
					_phase += Mathf.PI * 2 / Period * Time.deltaTime;
					BackEffect.color = new Color(c.r,c.g,c.b,_lLimit+(_mLimit-_lLimit)*(Mathf.Sin(_phase)+1)/2);
					break;
				case 2:
					BackEffect.color = new Color(c.r,c.g,c.b,0.3f);
					break;
				case 0:
					BackEffect.color = new Color(c.r,c.g,c.b,0.3f);
					break;
			}
		}
	}

	public void Reset()
	{
		SpriteRenderer = GetComponent<SpriteRenderer>();
		if (isStar)
		{
			BackEffect.gameObject.SetActive(true);
			if (stageNum <= 4)
				isHidden = false;
			else
				isHidden = true;

			state = GameManager.Instance.StageData[worldNum, stageNum];

			if (!isHidden)
			{
				switch (state)
				{
					case 0:
						SpriteRenderer.sprite = locked;
						break;
					case 1:
						SpriteRenderer.sprite = notCleared;
						break;
					case 2:
					case 3:
						SpriteRenderer.sprite = cleared;
						break;
				}
			}
			else
			{
				switch (state)
				{
					case 0:
						SpriteRenderer.sprite = hiddenLocked;
						break;
					case 1:
						SpriteRenderer.sprite = hiddenNotCleared;
						break;
					case 2:
					case 3:
						SpriteRenderer.sprite = hiddenCleared;
						break;
				}
			}
		}
		else
		{
			BackEffect.gameObject.SetActive(false);
			SpriteRenderer.sprite = placeholder;
		}
	}
}
