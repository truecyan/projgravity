﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZodiacCtrl : MonoBehaviour
{
	[HideInInspector] public List<Vector2>[] CoordData = new List<Vector2>[12];
	[HideInInspector] public List<Vector2Int>[] LinkData = new List<Vector2Int>[12];
	[HideInInspector] public List<StarProperties>[] StarData = new List<StarProperties>[12];
	[HideInInspector] public List<Line>[] LineData = new List<Line>[12]; 
	public GameObject StarPrefab;
	public GameObject LinePrefab;
	public GameObject SelectorPrefab;
	public int scaleUnit = 60;
	public int focusScale = 5;
	public int unfocusScale = 4;
	public int currNum = 0;
	public float swipeTime = 0.5f;
	public Text WorldNameText;
	public Text StageNameText;
	public Text StageStatusText;
	public GameObject StageSelectorUI;
	public int SelectedStage;

	private GameObject _selector;
	public readonly string[] WorldNameKR =
	{
		"01/물고기자리", "02/물병자리", "03/염소자리", "04/사수자리", "05/전갈자리", "06/천칭자리", "07/처녀자리", "08/사자자리", "09/게자리", "10/쌍둥이자리",
		"11/황소자리", "12/양자리"
	}; //월드 이름 한글

	public readonly string[] StageStatus =
	{
		"Locked", "Not Cleared", "Cleared", "Perfectly Cleared"
	};
	private bool _isMoving;
	private GameObject _prevGroup;
	private GameObject _currGroup;
	private GameObject _nextGroup;

	// Use this for initialization
	void Start ()
	{
		if (GameManager.Instance.WorldNum != -1)
		{
			currNum = GameManager.Instance.WorldNum;
			SelectedStage = GameManager.Instance.StageNum;
		}
		WorldNameText.text = WorldNameKR[currNum];
		for (int i = 0; i < 12; i++)
		{
			CoordData[i] = new List<Vector2>();
			LinkData[i] = new List<Vector2Int>();
			StarData[i] = new List<StarProperties>();
			LineData[i] = new List<Line>();
		}
		for (int j = 0; j < 12; j++)
		{
			var rawData = CSVReader.Read(String.Format("Zodiac/{0:00}",j+1));
			for (int i = 0; i < rawData.Count; i++)
			{
				CoordData[j].Add(new Vector2(float.Parse(rawData[i]["x"].ToString())/scaleUnit, -float.Parse(rawData[i]["y"].ToString())/scaleUnit));
				if (rawData[i]["start"].ToString() != "")
				{
					LinkData[j].Add(new Vector2Int((int)rawData[i]["start"],(int)rawData[i]["end"]));
				}
			}
		}
		_prevGroup = new GameObject();
		_prevGroup.transform.parent = transform;
		_prevGroup.transform.localPosition = new Vector3(-35,0,0);
		Create((currNum+11)%12,_prevGroup, false);
		_currGroup = new GameObject();
		_currGroup.transform.parent = transform;
		_currGroup.transform.localPosition = new Vector3(0,0,0);
		Create((currNum)%12,_currGroup, true);
		_nextGroup = new GameObject();
		_nextGroup.transform.parent = transform;
		_nextGroup.transform.localPosition = new Vector3(35,0,0);
		Create((currNum+1)%12,_nextGroup, false);
		for (int i = 0; i < 10; i++)
		{
			StarData[currNum][i].isStar = true;
		}

		_selector = Instantiate(SelectorPrefab);
		Mark();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonUp("Horizontal") && !_isMoving)
		{
			if (Input.GetAxis("Horizontal") > 0)
			{
				RightStage();
			}
			else
			{
				LeftStage();
			}
		}
		if (Input.GetButtonUp("Next"))
		{
			RightButton();
		}
		if (Input.GetButtonUp("Prev"))
		{
			LeftButton();
		}
		if (Input.GetButtonUp("Submit"))
		{
			StartGame();
		}

		if (Input.GetButtonUp("Cancel"))
		{
			Initiate.Fade("MainScene", Color.black, 2.0f);
		}
	}

	public void LeftButton()
	{
		if (_isMoving) return;
		for (int i = 0; i < 10; i++)
		{
			StarData[currNum][i].isStar = false;
			StarData[currNum][i].Reset();
		}
		var newGroup = new GameObject();
		newGroup.transform.parent = transform;

		newGroup.transform.localPosition = new Vector3(-70,0,0);
		currNum = (currNum+11)%12;
		Create((currNum+11)%12,newGroup, false);
		var removingGroup = _nextGroup;
		_nextGroup = _currGroup;
		_currGroup = _prevGroup;
		_prevGroup = newGroup;
		StartCoroutine(Move(removingGroup, false));
	}

	public void RightButton()
	{
		if (_isMoving) return;
		for (int i = 0; i < 10; i++)
		{
			StarData[currNum][i].isStar = false;
			StarData[currNum][i].Reset();
		}
		var newGroup = new GameObject();
		newGroup.transform.parent = transform;

		newGroup.transform.localPosition = new Vector3(70,0,0);
		currNum = (currNum+1)%12;
		Create((currNum+1)%12,newGroup, false);
		var removingGroup = _prevGroup;
		_prevGroup = _currGroup;
		_currGroup = _nextGroup;
		_nextGroup = newGroup;
		StartCoroutine(Move(removingGroup,true));
	}

	public void LeftStage()
	{
		if(SelectedStage > 0)
			SelectedStage--;
		Mark();
	}
	public void RightStage()
	{
		if(SelectedStage < 4)
			SelectedStage++;
		Mark();
	}

	public void StartGame()
	{
		GameManager.Instance.StageStart(currNum, SelectedStage);
	}

	void Create(int num, GameObject group, bool isFocused)
	{
		for (int i = 0; i < CoordData[num].Count; i++)
		{
			var star = Instantiate(StarPrefab, group.transform, true);
			if(!isFocused)
				star.transform.localPosition = CoordData[num][i]*unfocusScale;
			else
				star.transform.localPosition = CoordData[num][i]*focusScale;
			var starInfo = star.GetComponent<StarProperties>();
		
			if (i < 10)
			{
				starInfo.worldNum = num;
				starInfo.stageNum = i;
			}
			StarData[num].Add(starInfo);
		}
		for (int i = 0; i < LinkData[num].Count; i++)
		{
			var lineObj = Instantiate(LinePrefab, group.transform, true);
			var newLine = lineObj.GetComponent<Line>();
			LineData[num].Add(newLine);
			newLine.Initialise(StarData[num][LinkData[num][i].x].transform.position,StarData[num][LinkData[num][i].y].transform.position,0.04f,Color.white);
		}
	}


	IEnumerator Move(GameObject remo, bool toLeft)
	{
		WorldNameText.text = "";
		StageSelectorUI.SetActive(false);
		_selector.SetActive(false);
		float transition = 0;
		_isMoving = true;
		while (true)
		{
			transition += Time.deltaTime / swipeTime;
			if (toLeft)
			{
				_prevGroup.transform.Translate(-35*Time.deltaTime/swipeTime,0,0);
				_currGroup.transform.Translate(-35*Time.deltaTime/swipeTime,0,0);
				_nextGroup.transform.Translate(-35*Time.deltaTime/swipeTime,0,0);
				Scale(currNum,transition*focusScale+(1-transition)*unfocusScale);
				Scale((currNum+11)%12,transition*unfocusScale+(1-transition)*focusScale);
				remo.transform.Translate(-35*Time.deltaTime/swipeTime,0,0);
				if (_currGroup.transform.position.x < 0)
				{
					_prevGroup.transform.localPosition = new Vector3(-35,0,0);
					_currGroup.transform.localPosition = new Vector3(0,0,0);
					_nextGroup.transform.localPosition = new Vector3(35,0,0);
					StarData[(currNum+10)%12] = new List<StarProperties>();
					LineData[(currNum+10)%12] = new List<Line>();
					Destroy(remo);
					break;
				}
			}
			else
			{
				_prevGroup.transform.Translate(35*Time.deltaTime/swipeTime,0,0);
				_currGroup.transform.Translate(35*Time.deltaTime/swipeTime,0,0);
				_nextGroup.transform.Translate(35*Time.deltaTime/swipeTime,0,0);
				remo.transform.Translate(35*Time.deltaTime/swipeTime,0,0);
				Scale(currNum,transition*focusScale+(1-transition)*unfocusScale);
				Scale((currNum+1)%12,transition*unfocusScale+(1-transition)*focusScale);
				if (_currGroup.transform.position.x > 0)
				{
					_prevGroup.transform.localPosition = new Vector3(-35,0,0);
					_currGroup.transform.localPosition = new Vector3(0,0,0);
					_nextGroup.transform.localPosition = new Vector3(35,0,0);
					StarData[(currNum+2)%12] = new List<StarProperties>();
					LineData[(currNum+2)%12] = new List<Line>();
					Destroy(remo);
					break;
				}
			}
			yield return new WaitForEndOfFrame();
		}

		SelectedStage = 0;
		for (int i = 0; i < 10; i++)
		{
			StarData[currNum][i].isStar = true;
			StarData[currNum][i].Reset();
		}

		Mark();
		WorldNameText.text = WorldNameKR[currNum];
		_isMoving = false;
		StageSelectorUI.SetActive(true);
		_selector.SetActive(true);
	}

	void Mark()
	{
		_selector.transform.position = StarData[currNum][SelectedStage].gameObject.transform.position;
		StageNameText.text = GameManager.Instance.StageName[SelectedStage];
		var status = GameManager.Instance.StageData[currNum,SelectedStage];
		StageStatusText.text = StageStatus[status];
	}
	void Scale(int num, float scale)
	{
		for (int i = 0; i < StarData[num].Count; i++)
		{
			StarData[num][i].transform.localPosition = CoordData[num][i]*scale;
		}
		for (int i = 0; i < LineData[num].Count; i++)
		{
			LineData[num][i].Initialise(StarData[num][LinkData[num][i].x].transform.position,StarData[num][LinkData[num][i].y].transform.position,0.04f,Color.white);
		}
	}
}
