﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FpsDisplayer : MonoBehaviour
{

	public Text fps;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		fps.text = Mathf.Round(1 / Time.deltaTime).ToString();
	}
}
