﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainSceneEffect : MonoBehaviour {

	[HideInInspector] public GameObject[] Planets;
	[HideInInspector] public Rigidbody2D Rigidbody;
	[HideInInspector] public SpriteRenderer Renderer;
	[HideInInspector] public Animator Animator;
	[HideInInspector] public PlayerProperties PlayerInfo; //플레이어 체력,추진게이지 등의 정보를 담고 있음

	[Header("Environment Constants")]
	public float GravityConstant = 2.0f;
	public float RotConstant = 5.0f;
	public float GroundRotConstant = 30.0f;

	[Header("Player Movement Constants")]
	public float JumpConstant = 18.0f;

	[Header("Debug")]
	public float GravityForce;
	public float PlayerSpeed;

	private bool _gameStart = false;

	[SerializeField] private GameObject _currPlanet = null;
	[SerializeField] private GameObject _jumpPlanet = null;
	[SerializeField] private bool _colCheck = false;
	[SerializeField] private bool _onObject = false;


	private Vector3 _jumpPoint = Vector3.positiveInfinity;
	private Vector3 _exitPoint = Vector3.positiveInfinity;


	// Use this for initialization
	void Start()
	{
		//플레이어 속성 받아오기
		PlayerInfo = gameObject.GetComponent<PlayerProperties>();

		//맵에 존재하는 모든 행성 받아오기
		Planets = GameObject.FindGameObjectsWithTag("Planet");

		//컴포넌트 받아오기
		Rigidbody = gameObject.GetComponent<Rigidbody2D>();
		Rigidbody.mass = 1; //속성 적용
		Renderer = gameObject.GetComponent<SpriteRenderer>();
		Animator = gameObject.GetComponent<Animator>();

		Animator.SetBool("isRunning", true);

		CannonCtrl._doTutorial = true;
	}
	private void OnCollisionEnter2D(Collision2D other)
	{
		//착지 시
		_colCheck = true;

		//애니메이션 - 점프/추진 상태 취소
		Animator.SetBool("isJumping", false);

		_currPlanet = other.gameObject;
		Rigidbody.velocity = Vector2.zero;

	}

	private void OnCollisionStay2D(Collision2D other)
	{
		//가로 이동
		//Rigidbody.velocity = (transform.right*Input.GetAxis("Horizontal")*5);

		if (_colCheck == false)
		{
			_colCheck = true;
		}
	}

	private void OnCollisionExit2D(Collision2D other)
	{

		_colCheck = false;
		_onObject = false;
		_exitPoint = transform.position;
		//_currPlanet = null;
		//Debug.Log("collision exit");
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		//점프 후 콜라이더 복구 (일정 거리 이상 떨어질 시)
		if (_jumpPlanet != null && (transform.position - _jumpPoint).magnitude > 0.1f)
		{
			//_jumpPlanet.GetComponent<CircleCollider2D>().enabled = true;
			Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Planet"), false);
			_jumpPlanet = null;
			//_jumpPoint = Vector3.positiveInfinity;
		}

		if (_currPlanet != null)
		{
			//충돌하지 않은 상태에서 행성에서 일정 거리 이상 떨어질 경우 현재 행성 정보 초기화
			if (!_colCheck && (transform.position - _exitPoint).magnitude > 0.2f)
			{
				//Debug.Log("Initialized");
				_currPlanet = null;
				//_exitPoint = Vector3.positiveInfinity;
			}

		}

		if (_currPlanet != null || _colCheck)
		{
			//가로이동(지상)
			Rigidbody.velocity = (transform.right * PlayerInfo.MoveSpeed *0.7f);
		}

		//중력 계산
		Vector2 totalForce = Vector2.zero;
		foreach (var planet in Planets)
		{
			Vector2 pos = planet.transform.position;
			float planetMass = planet.GetComponent<PlanetProperties>().PlanetMass;
			Vector2 distVector = pos - Rigidbody.position;
			totalForce += GravityConstant * distVector.normalized * planetMass / Sqr(distVector.magnitude);
		}

		//플레이어 기준 벡터 (앞, 아래)
		Vector3 fwdVector = transform.right;
		Vector3 dwnVector = -transform.up;

		//중력 적용
		Vector3 forceVector = totalForce;
		GravityForce = totalForce.magnitude;
		PlayerSpeed = Rigidbody.velocity.magnitude;

		//애니메이션 - 추락 판정 : 플레이어 속도와 중력 방향이 같을것, 플레이어의 발쪽으로 떨어질것, 착지해있지 않을것, 추진하지 않을것
		if (//Vector2.Dot(totalForce, Rigidbody.velocity) > 0 && 
			Vector2.Dot(Convert3Dto2D(dwnVector), Rigidbody.velocity) > 0
			&& (transform.position - _exitPoint).magnitude > 2f
			&& !_colCheck
			&& _currPlanet == null
			&& !Animator.GetBool("isBoosting"))
		{
			Animator.SetBool("isFalling", true);
		}
		else
		{
			Animator.SetBool("isFalling", false);
		}

		//과도한 회전을 막기 위한 벡터(초기, z값)
		float initCross = Vector3.Cross(dwnVector, forceVector).z;

		//플레이어 회전
		float rot;
		if (_colCheck)
			rot = Vector2.Dot(totalForce, fwdVector) * GroundRotConstant * Time.deltaTime;
		else
			rot = Vector2.Dot(totalForce, fwdVector) * RotConstant * Time.deltaTime;
		transform.Rotate(0f, 0f, rot);

		//과도하게 회전할 시 중력 방향으로 플레이어의 회전방향을 맞춤
		dwnVector = -gameObject.transform.up;
		float newCross = Vector3.Cross(dwnVector, forceVector).z;
		if (initCross * newCross < 0)
		{
			float nowAngle = Mathf.Atan2(dwnVector.y, dwnVector.x) * Mathf.Rad2Deg;
			float toAngle = Mathf.Atan2(forceVector.y, forceVector.x) * Mathf.Rad2Deg;
			rot = toAngle - nowAngle;
			transform.Rotate(0f, 0f, rot);
		}
		Rigidbody.AddForce(totalForce);

		
		
		
		//게임 시작
		if (_gameStart)
		{
			if (_currPlanet != null)
			{
				Jump();
				Initiate.Fade("StageSelect", Color.black, 2.0f);
			}
		}

	}

	void Update()
	{
		if (Input.GetButtonUp("Submit"))
		{
			GameStart();
		}
	}
	public void GameStart()
	{
		_gameStart = true;
	}

	private void Jump()
	{
		//점프 한 위치, 행성 저장
		_jumpPoint = transform.position;
		_jumpPlanet = _currPlanet;
		_currPlanet = null;

		Rigidbody.AddForce(gameObject.transform.up * JumpConstant, ForceMode2D.Impulse);

		//점프 애니메이션 실행
		Animator.SetBool("isJumping", true);
		Animator.SetTrigger("doJump");

		//레이어 충돌 무시 설정 켜기
		if (!_onObject)
			Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Planet"), true);
		else
			Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Object"), true);
	}

	float Sqr(float a)
	{
		return a * a;
	}

	Vector2 Convert3Dto2D(Vector3 v)
	{
		return new Vector2(v.x, v.y);
	}
}
