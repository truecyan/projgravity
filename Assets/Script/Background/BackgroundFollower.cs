﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BackgroundFollower : MonoBehaviour
{
	[HideInInspector] public Transform Camera;
	[HideInInspector] public SpriteRenderer Renderer;

	// Use this for initialization
	void Start ()
	{
		Camera = GameObject.FindGameObjectWithTag("MainCamera").transform;
		Renderer = GetComponent<SpriteRenderer>();
		BackgroundScroller parent = transform.parent.GetComponent<BackgroundScroller>();
		int idx = BackgroundScroller.random.Next(parent.Backgrounds.Length);
		Renderer.sprite = parent.Backgrounds[idx];
	}
	
	// Update is called once per frame
	void Update () {
		if (Camera.position.x - transform.position.x > 60f)
		{
			transform.position += new Vector3(120,0,0);
		}
		if (Camera.position.x - transform.position.x < -60f)
		{
			transform.position += new Vector3(-120,0,0);
		}
		if (Camera.position.y - transform.position.y > 60f)
		{
			transform.position += new Vector3(0,120,0);
		}
		if (Camera.position.y - transform.position.y < -60f)
		{
			transform.position += new Vector3(0,-120,0);
		}
	}
}
