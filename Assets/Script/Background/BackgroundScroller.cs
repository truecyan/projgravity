﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class BackgroundScroller : MonoBehaviour
{
	public static Random random = new Random();

	[HideInInspector] public Transform Camera;
	[HideInInspector] public Vector2 InitialVector;
	
	public float FollowRate = 0.2f;
	public Sprite[] Backgrounds;

	// Use this for initialization
	void Start ()
	{
		Camera = GameObject.FindGameObjectWithTag("MainCamera").transform;
		InitialVector = Camera.position;
		transform.position = InitialVector;
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.position = (new Vector2(Camera.position.x,Camera.position.y) - InitialVector) * FollowRate + InitialVector;
	}
}
