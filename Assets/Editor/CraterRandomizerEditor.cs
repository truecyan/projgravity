﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(CraterRandomizer))]
[CanEditMultipleObjects]

public class CraterRandomizerEditor : Editor{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CraterRandomizer myScript = (CraterRandomizer) target;
        if (GUILayout.Button("Generate"))
        {
            myScript.Generate();
        }
        if (GUILayout.Button("Remove"))
        {
            myScript.Remove();
        }
    }
}
