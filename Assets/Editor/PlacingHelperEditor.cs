﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(PlacingHelper))]
[CanEditMultipleObjects]

public class PlacingHelperEditor : Editor{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PlacingHelper myScript = (PlacingHelper) target;
        if (GUILayout.Button("Adjust Object to Nearest Planet"))
        {
            myScript.Adjust();
        }
    }
}
