﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(CraterSpriteManager))]
[CanEditMultipleObjects]

public class CraterSpriteManagerEditor : Editor{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CraterSpriteManager myScript = (CraterSpriteManager) target;
        if (GUILayout.Button("Apply Change"))
        {
            myScript.ImageSet();
        }
    }
}
