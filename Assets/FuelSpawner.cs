﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelSpawner : MonoBehaviour {

	public FuelProperties fuelPrefab;
	public float FuelAmount = 100;
	public float spawnDistance = 15;

	private FuelProperties fuel;
	private PhysicalObject player;
	private PlayerMovement playerMovement;

	// Use this for initialization
	void Start () {
		GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
		player = playerObject.GetComponent<PhysicalObject>();
		playerMovement = playerObject.GetComponent<PlayerMovement>();

		StartCoroutine(SpawnCoroutine());
	}

	public void Spawn()
	{
		if (fuel == null)
		{
			fuel = Instantiate<FuelProperties>(fuelPrefab, transform.position, transform.rotation);
			fuel.Amount = FuelAmount;
		}
	}

	private IEnumerator SpawnCoroutine()
	{
		while (true)
		{
			while (GetPlayerDistance() < spawnDistance)
			{
				yield return null;
				Spawn();
				while (!playerMovement.statePropel)
					yield return null;
				while (!player.IsLanding())
					yield return null;
			}
			yield return null;
		}	
	}

	private float GetPlayerDistance()
	{
		float dist = (player.transform.position - transform.position).magnitude;
		return dist;
	}
}
